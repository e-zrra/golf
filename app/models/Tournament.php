<?php

class Tournament extends \Eloquent {
	
	protected $fillable = ['name', 'start_date', 'finish_date', 'description', 'status', 'gender_id'];

    /**
     * Status => 0 : Active, 1 : Finished, 2 : Cancel
     */

	protected $table = 'tournaments';

	public static $rules_validate = array(
		'name' 	=> 'required'
	);

	public static $rules_messages = array(
	    'name.required' => 'El nombre es requerido'
	);

    /**
     * Check if the player is signed in the tournament
     * @return boolean
     */
    public function is_subscribed () {
        
        $user = Auth::user();
            
        $subscribed = UserTournament::where('user_id', $user->id)->where('tournament_id', $this->id)->first();

        if ($subscribed) return true;

        return false;
    }

    public function stage () {

        return $this->hasMany('Stage');
    }

    public function gender () {
        
        return $this->belongsTo('Gender');
    }
}