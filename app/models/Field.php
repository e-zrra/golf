<?php

class Field extends \Eloquent {
	
	protected $fillable = ['name', 'description', 'place'];

	protected $table = 'fields';

	public static $rules_validate = array(
		'name' 	=> 'required',
		'place'     => 'required'
	);

	public static $rules_messages = array(
	    'name.required' => 'El nombre es requerido',
	    'place.required' => 'El campo lugar es requerido'
	);
}