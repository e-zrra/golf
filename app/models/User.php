<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use SoftDeletingTrait;
	
	protected $dates = ['deleted_at'];

	public function full_name () {

		return $this->first_name . ' ' . $this->last_name;
	}

	use UserTrait, RemindableTrait;

	protected $fillable = ['first_name', 'last_name', 'email'];

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	public static function unique_user ($email, $user_id)
	{
		$user = User::where('email', $email)->where('id', '<>', $user_id)->first();

		if ($user) throw new Exception("El correo electrónico ya se encuentra en uso");
	}

	/**
	 * Create rules
	 *
	 * @var array
	 */
	public static $create_validate = array(
		'first_name' 	=> 'required',
		'last_name'     => 'required',
		'email'     	=> 'required',
		'password'     	=> 'required'
	);

	public static $edit_validate = array(
		'first_name' 	=> 'required',
		'last_name'     => 'required',
		'email'     	=> 'required'
	);

	public static $create_messages = array(
	    'first_name.required' => 'El nombre es requerido',
	    'last_name.required' => 'Los apellidos son requeridos',
	    'email.required' => 'El correo electrónico es requerido',
	    'password.required' => 'La contraseña es requerida'
	);

	public static $edit_messages = array(
	    'first_name.required' => 'El nombre es requerido',
	    'last_name.required' => 'Los apellidos son requeridos',
	    'email.required' => 'El correo electrónico es requerido'
	);

	/* public function player ()
	{
		return Player::where('user_id', $this->id)->first();
	} */

	public function player () {

		return $this->belongsTo('Player');
	}

}
