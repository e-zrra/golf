<?php

class Stage extends \Eloquent {
	
	protected $fillable = ['tournament_id', 'description', 'field_id', 'number', 'start_date', 'finish_date'];

	protected $table = 'stages';

	public static $rules_validate = array(
		'field_id' 	=> 'required'
	);

	public static $rules_messages = array(
	    'field_id.required' => 'El campo es requerido'
	);

	public function field () {
		
		return $this->belongsTo('Field');
	}

	/**
	 * Lists stage relation of user_stage
	 * User variable is a player
	 * No data 	-> 
	 * Status 	-> 0 => Started
	 * Status 	-> 1 => Finished
	 * @return number
	 */
	public function stage_user () {

		$user = Auth::user();

		$user_stage = UserStage::where('user_id', $user->id)->where('stage_id', $this->id)->first();

		if ($user_stage)
		{
			return $user_stage;
		}

		return null;
	}
}