<?php

class Tutor extends \Eloquent {
	
	protected $fillable = ['first_name', 'last_name', 'email', 'phone_number'];

	protected $table = 'tutors';

	function full_name () {

		return $this->first_name . ' ' . $this->last_name;
	}

	public static $rules_validate = array(
		'first_name' 	=> 'required',
		'last_name' 	=> 'required'
	);

	public static $rules_messages = array(
	    'first_name.required' => 'El nombre es requerido',
	    'last_name.required' => 'Apellidos son requeridos'
	);
}

