<?php

class UserTournament extends \Eloquent {
	
	/**
	 * Status 0 => Iniciado, 1 => Finalizado
	 */
	protected $fillable = ['tournament_id', 'user_id', 'status'];

	protected $table = 'user_tournaments';

    public function user () {

        return $this->belongsTo('User');
    }

    public function tournament () {

    	return $this->belongsTo('Tournament');
    }

    public function user_is_stage ($user_id, $stage_id) {

    	$data = UserStage::where('stage_id', $stage_id)->where('user_id', $user_id)->first();

    	if ($data) {
    		
    		if ($data->status == 0) {
    			
    		}

    		return true;
    	}

    	return false;

    }

}
