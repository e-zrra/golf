<?php

class Gender extends \Eloquent {
	
	protected $fillable = ['name', 'description'];

	protected $table = 'genders';
}