<?php

class Category extends \Eloquent {
	
	protected $fillable = ['name', 'color', 'start_hole', 'finish_hole', 'holes', 'description'];

	protected $table = 'categories';
}