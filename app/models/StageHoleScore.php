<?php

class StageHoleScore extends \Eloquent {
	
	protected $fillable = ['hole', 'yard', 'place', 'user_id', 'stage_id', 'day', 'day_date', 'user_stage_id'];

	protected $table = 'stage_hole_score';
}