<?php

class UserStage extends \Eloquent {
		
	protected $fillable = ['stage_id', 'tournament_id', 'start_hole', 'hour', 'position', 'status', 'description', 'category_id', 'user_id'];

	protected $table = 'user_stages';

    public function stage ()
    {
        return $this->hasOne('Stage');
    }

    public static $create_validate = array(
		'stage_id' 	=> 'required',
		'tournament_id'     => 'required',
		'user_id'     	=> 'required',
		'start_hole'     	=> 'required'
	);
	public static $create_messages = array(
	    'stage_id.required' => 'La etapa es requerida',
	    'tournament_id.required' => 'Torneo es requerido',
	    'user_id.required' => 'Jugador es requerido',
	    'start_hole.required' => 'Hoyo de inicio requerida'
	);
}