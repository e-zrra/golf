<?php

class Player extends \Eloquent
{
	protected $fillable = ['phone_number', 'birthday', 'birthplace', 'allergies', 'other_sports', 'category_id'];

	public function category ()
	{
		return $this->belongsTo('Category');
	}
}