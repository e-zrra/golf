<!DOCTYPE html>
<html>
<head>
	<title>Panel de Administrador</title>
	<link rel="stylesheet" type="text/css" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	
	{{  stylesheet_link_tag('panel/application.css') }}

	<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="{{ url('assets/stylesheets/panel/sidebar.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ url('assets/stylesheets/panel/style.css') }}" />
	
	{{ javascript_include_tag('panel/application.js') }}

</head>
<body>
	<!-- <div class="message-notification" id="message-notification"> Mensaje </div> -->
	<nav class="navbar navbar-default navbar-fixed-top">
      	<div class="container">
        	<div class="navbar-header">
          		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		            <span class="sr-only">Toggle navigation</span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
          		</button>
          		<a class="navbar-brand" href="">
					<img src="{{ url('assets/images/golfnoroeste-logo-png.png') }}" width="120px" class="img-responsive" />
				</a>
        	</div>

        	<div class="collapse navbar-collapse" id="">
	          	<ul class="nav navbar-nav navbar-right">
	          		<li>
	          			<a href="{{ route('logout') }}" class="ion-log-out"> Cerrar sesión</a>
	          		</li>
	          	</ul>
	        </div>
      	</div>
    </nav>
    <div class="container-fluid">
      	<div class="row">
	        <div class="col-sm-2 col-md-2 col-md-offset-1 sidebar">
	          	<ul class="nav nav-sidebar">
		            <li class="{{ nav_option_active(array('/', '')) }}">
		            	<a href="{{ route('panel.admin.index') }}"> <i class="icon ion-home"></i> Inicio <span class="sr-only">(current)</span></a>
		            </li>
		            <li class="{{ nav_option_active(array('players')) }}">
		            	<a href="{{ route('panel.admin.players.index') }}">
		            		<i class="icon ion-person-stalker"></i> Jugadores
		            	</a>
		            </li>
		            <li class="{{ nav_option_active(array('fields')) }}">
		            	<a href="{{ route('panel.admin.fields.index') }}"> <i class="icon ion-leaf"></i> Campos</a>
		            </li>
		            <li class="{{ nav_option_active(array('tournaments')) }}">
		            	<a href="{{ route('panel.admin.tournaments.index') }}"> <i class="icon ion-bonfire"></i> Torneos</a>
		            </li>
	          	</ul>
	          	<!-- 
	          	<ul class="nav nav-sidebar">
		            <li><a href="#"> <i class="icon ion-gear-a"></i> Configuración</a></li>
	          	</ul>
	          	<ul class="nav nav-sidebar">
		            <li><a href="">Nav item again</a></li>
		            <li><a href="">One more nav</a></li>
		            <li><a href="">Another nav item</a></li>
	          	</ul>-->
	        </div>
	        <div class="col-sm-9 col-sm-offset-3 col-md-8 col-md-offset-3 main">
	        	@if (Session::has('message'))
				    <div class="alert {{ Session::get('alert-class') }}">
				    	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				    	{{ Session::get('message') }}
				    </div>
				@endif
				
	        	@yield('content')
	        </div>
      	</div>
    </div>
	
	<!-- <footer class="footer">
      	<div class="copyright">
	      	<div class="container">
	      		<div class="row">
	      			<div class="col-md-12">
	      				Copyright 2015 Golf | Todos los derechos reservados
	      			</div>
	      		</div>
	      	</div>
      	</div>
    </footer> -->

    <script type="text/javascript" src="{{ url('assets/javascripts/panel/helpers.js') }}"></script>
</body>
</html>