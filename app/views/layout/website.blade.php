<!DOCTYPE html>
<html lang="es-MX">
	<head>
		<title>Golf</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="keywords" content="" /> 
		{{  stylesheet_link_tag('website/application.css') }}
		<link rel="stylesheet" type="text/css" href="{{ url('assets/stylesheets/website/style.css') }}">
		<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500' rel='stylesheet' type='text/css'>
		<link rel="icon" type="image/png" href="" sizes="32x32">
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		{{ javascript_include_tag('website/application.js') }}

		<style>
			.hero-signin { background: url("{{ url('assets/images/golf3.jpg') }}") no-repeat; }
			.hero-signup { background: url("{{ url('assets/images/golf2.jpg')  }}") no-repeat; }
		</style>
	</head>

	<body data-spy="scroll" data-target=".main-nav">
		<header class="st-navbar">
			<nav class="navbar navbar-default navbar-website navbar-fixed-top clearfix bg-tranparent">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sept-main-nav">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="">
							<img src="assets/images/golfnoroeste-logo-png.png" width="120px" class="img-responsive" />
						</a>
					</div>
					<div class="collapse navbar-website navbar-collapse main-nav" id="sept-main-nav">
						<ul class="nav navbar-nav navbar-right">
							<li class=""><a class="" href="{{ route('website.index') }}"> Inicio </a></li>
							@if(Auth::check())
								<li>
									<a href="{{ route('session.user_panel') }}"> 
										Perfil <i class="fa fa-cog fa-sm"></i>
									</a>
								</li>
							@else
								<li><a href="{{ route('session.signup') }}" class="">Registrarse</a></li>
								<li><a href="{{ route('session.signin') }}" class="">Iniciar sesion</a></li>
							@endif
						</ul>
					</div>
				</div>
			</nav>
		</header>
		
		@yield('content')

		<!-- <footer class="footer">
	      	<div class="copyright">
		      	<div class="container">
		      		<div class="row">
		      			<div class="col-md-12">
		      				Copyright 2015 Golf | Todos los derechos reservados
		      			</div>
		      		</div>
		      	</div>
	      	</div>
	    </footer> -->

		<script type="text/javascript" src="{{ url('assets/javascripts/website/helpers.js') }}"></script>
	</body>
</html>
