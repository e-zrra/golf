@extends('layout.admin')
@section('content')

<!-- Modal Enable User Stage -->
<div class="modal fade" id="modal_user_stage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  	<div class="modal-dialog" role="document">
	    <div class="modal-content">
	    	{{ Form::open(array('route' => 'panel.admin.user_stage.store', 'id' => 'form-user-stage')) }}
	      	<div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="myModalLabel">Habilitar etapa</h4>
	      	</div>
	      	<div class="modal-body">
	      	</div>
	      	<div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		        <input type="submit" class="btn btn-primary" value="Guardar"></input>
	      	</div>
	      	{{ Form::close() }}
	    </div>
  	</div>
</div>

<h3 class="m0 title">Historial</h3>
<hr />

<ol class="breadcrumb">
  	<li><a href="{{ route('panel.admin.index') }}">Inicio</a></li>
  	<li> <a href="{{ route('panel.admin.tournaments.index') }}"></a> Torneos</li>
  	<li class="active">Historial de torneo</li>
</ol>

@foreach($stages as $stage)

	<div class="panel panel-default">
		<div class="panel-heading">
			<b>Etapa:</b> {{ $stage->description }}
		</div>
		<div class="panel-body">
			
			<h4>Campo: </h4>
			{{ ($stage->field) ? $stage->field->name : '<i>No disponible</i>' }}

			<h4>Jugadores:</h4>
			<a role="button" data-toggle="collapse" href="#collapsePlayers{{ $stage->id }}" aria-expanded="false" aria-controls="collapsePlayers{{ $stage->id }}">
				<i class="ion-ios-people"></i> Ver jugadores
			</a>

			<div class="collapse" id="collapsePlayers{{ $stage->id }}">
			<hr>
				<div>
					<table class="table table-bordered">
						<thead>
							<th>
								Nombre
							</th>
							<th>
								Categoría
							</th>
							<th>
								Estatus
							</th>
						</thead>
						<tbody>
							@foreach($user_tournaments as $data)
								<tr>
									@if($data->user)
										<td width="300px">
											{{ $data->user->full_name }}
										</td>
										<td width="240px">
											@if($data->user->player)
												{{ ($data->user->player->category) ? $data->user->player->category->name : '<i>No disponible</i>' }}
											@endif
										</td>
										<td>
											@if($data->user_is_stage($data->user->id, $stage->id))
												<label class="label label-success">Habilitado</label>
											@else
												<label class="label label-warning mr10">No habilitado</label>
												<a src="{{ route('panel.admin.user_stage.create', array($stage->id, $stage->tournament_id, $data->user->id)) }}" class="mr10 pointer btn_user_stage" data-toggle="modal" data-target="#modal_user_stage"> <i class="ion-flag"></i> Habilitar etapada</a>
											@endif
										</td>
									@else
										<td colspan="4">
											Removido
										</td>
									@endif
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>

		</div>
	</div>

@endforeach

<script type="text/javascript" src="{{ url('assets/javascripts/panel/admin/user_stage.js') }}"></script>

@stop