<div class="form-group">
	<label class="start_hole">Hoyo de salida</label>
	{{ Form::text('start_hole', null, array('class' => 'form-control required')) }}
</div>

<div class="form-group">
	<label class="start_hole">Hora de salida</label>
	{{ Form::text('hour', null, array('class' => 'form-control required datetime-time-lt')) }}
</div>

{{ Form::hidden('stage_id', $stage_id) }}
{{ Form::hidden('tournament_id', $tournament_id) }}
{{ Form::hidden('user_id', $user_id) }}

<script type="text/javascript" src="{{ url('assets/javascripts/panel/helpers.js') }}"></script>