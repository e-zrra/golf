@extends('layout.admin')
@section('content')
	
	<div class="row">
		<div class="col-md-6">
			<h3 class="m0 title">Etapas del torneo </h3>
			<h4><b class="color-black">{{ $tournament->name }}</b></h4>
		</div>
		<div class="col-md-6" align="right"> 
			<a href="{{ route('panel.admin.tournaments.show', $tournament->id) }}" class="mr10"><i class="icon ion-bonfire"></i> Ir a detalles del torneo</a>
			<a href="{{ route('panel.admin.stages.create', $tournament->id) }}" class="btn btn-default-success">
				<i class="ion-plus-round"></i> Agregar nueva etapa
			</a>
		</div>
	</div>
	<hr/>
	<ol class="breadcrumb">
	    <li><a href="{{ route('panel.admin.index') }}">Inicio</a></li>
	    <li><a href="{{ route('panel.admin.tournaments.index') }}">Torneos</a></li>
	    <li class="active">Etapas</li>
	</ol>
	<table class="table table-bordered">
		<thead>
			<th width="50px">Numero</th>
			<th>
				Descripción
			</th>
			<th>
				Campo
			</th>
			<th width="200px">
			</th>
		</thead>
		<tbody>
			@foreach($stages as $stage)
				<tr>
					<td align="center">{{ $stage->number }}</td>
					<td>
						{{ $stage->description }}
					</td>
					<td>
						{{ ($stage->field) ? $stage->field->name : '<i>Sin registro</i>' }}
					</td>
					<td align="center">
						<div class="btn-group" role="group">
						  	{{ Form::open(array('method' => 'DELETE', 'route' => array('panel.admin.stages.destroy', $stage->id))) }}
						  		<a href="{{ route('panel.admin.stages.edit', [$tournament->id,$stage->id]) }}" class="btn btn-default"> <i class="ion-gear-a"></i> Editar</a>
						  		<a class="destroy-record btn btn-default"> <i class="ion-trash-a"></i> Eliminar</a>
						  	{{ Form::close() }}
						</div>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	@if($stages->count() == 0)
		<i>Aún sin registros</i>
	@endif
	{{ $stages->links() }}

@stop