@extends('layout.admin')
@section('content')

{{ Form::model($stage, array('route' => array('panel.admin.stages.update', $stage->id), 'autocomplete' => 'off', 'id' => 'form-stages', 'method' => 'PUT')) }}

<div class="row">
	<div class="col-md-8">
		<h3 class="m0 title">Editar etapa del torneo <b class="color-black">{{ $tournament->name }}</b></h3>
	</div>
	<div class="col-md-4" align="right">
		{{  Form::submit('Guardar cambios', array('class' => 'btn btn-success mr10')) }}
		<a href="{{ redirect_back() }}">Regresar</a>
	</div>
</div>
<hr />
@include('panel.admin.stages._form')

{{ Form::close() }}

<script type="text/javascript" src="{{ url('assets/javascripts/panel/admin/stages.js') }}"></script>

@stop