@extends('layout.admin')
@section('content')
	
{{ Form::model($stage, array('route' => array('panel.admin.stages.store', $tournament->id, $stage->id), 'autocomplete' => 'off', 'id' => 'form-stages', 'method' => 'POST')) }}

<div class="row">
	<div class="col-md-9">
		<h3 class="m0 title">Nueva etapa para el torneo <b class="color-black">{{ $tournament->name }}</b></h3>
	</div>
	<div class="col-md-3" align="right">
		{{  Form::submit('Guardar', array('class' => 'btn btn-success mr10')) }}
		<a href="{{ redirect_back() }}">Regresar</a>
	</div>
</div>
<hr/>
<ol class="breadcrumb">
    <li><a href="{{ route('panel.admin.index') }}">Inicio</a></li>
    <li><a href="{{ route('panel.admin.tournaments.index') }}">Torneos</a></li>
    <li><a href="{{ route('panel.admin.stages.index', $tournament->id, $stage->id) }}">Etapas</a></li>
    <li class="active">Nuevo registro</li>
</ol>
@include('panel.admin.stages._form')

{{ Form::close() }}

<script type="text/javascript" src="{{ url('assets/javascripts/panel/admin/stages.js') }}"></script>

@stop