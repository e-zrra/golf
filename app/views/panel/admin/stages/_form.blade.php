<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('number', 'Numero de orden', array('class' => 'input-required', 'placeholder' => '')) }}
            {{ Form::text('number', null, array('class' => 'form-control required', 'placeholder' => '1, 2, o 3')) }}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('field_id', 'Campo', array('class' => 'input-required', 'placeholder' => '')) }}
            {{ Form::select('field_id', $fields, null, array('class' => 'form-control required')) }}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('start_date', 'Fecha de inicio', array('class' => 'input-required', 'placeholder' => '')) }}
            {{ Form::text('start_date', null, array('class' => 'form-control required datetime-date', 'placeholder' => '')) }}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('finish_date', 'Fecha de finalización', array('class' => '', 'placeholder' => '')) }}
            {{ Form::text('finish_date', null, array('class' => 'form-control datetime-date')) }}
        </div>
    </div>
</div>

<div class="form-group">
	{{ Form::label('description', 'Descripción', array('class' => '')) }}
	{{ Form::textarea('description', null, array('class' => 'form-control')) }}
</div>

