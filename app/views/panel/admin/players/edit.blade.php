@extends('layout.admin')
@section('content')

{{ Form::model($user, array('route' => array('panel.admin.players.update', $user->id), 'autocomplete' => 'off', 'id' => 'form-player-edit', 'method' => 'PUT')) }}

<div class="row">
	<div class="col-md-6">
		<h3 class="m0 title">Editar jugador</h3>
	</div>
	<div class="col-md-6" align="right">
		{{  Form::submit('Guardar cambios', array('class' => 'btn btn-success mr10')) }}
		<a href="{{ redirect_back() }}">Regresar</a>
	</div>
</div>
<hr />
<ol class="breadcrumb">
    <li><a href="{{ route('panel.admin.index') }}">Inicio</a></li>
    <li><a href="{{ route('panel.admin.players.index') }}">Jugadores</a></li>
    <li class="active">Editar registro</li>
</ol>
@include('panel.admin.players._form')

{{ Form::close() }}

<script type="text/javascript" src="{{ url('assets/javascripts/panel/admin/players.js') }}"></script>

@stop