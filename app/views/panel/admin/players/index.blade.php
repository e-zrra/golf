@extends('layout.admin')
@section('content')
	
	<div class="row">
		<div class="col-md-6">
			<h3 class="m0 title">Jugadores</h3>
		</div>
		<div class="col-md-6" align="right"> 
			<a href="{{ route('panel.admin.players.create') }}" class="btn btn-default-success">
				<i class="ion-plus-round"></i> Agregar nuevo jugador
			</a>
		</div>
	</div>
	<hr/>
	<ol class="breadcrumb">
      	<li><a href="{{ route('panel.admin.index') }}">Inicio</a></li>
      	<li class="active">Jugadores</li>
    </ol>
	<table class="table table-bordered">
		<thead>
			<th>
				Nombre
			</th>
			<th>
				Teléfono
			</th>
			<th width="200px">
			</th>
		</thead>
		<tbody>
			@foreach($users as $user)
				<tr>
					<td>
						{{ $user->full_name() }}
					</td>
					<td>
						@if($user->player)
							{{ $user->player->phone_number }}
						@endif
					</td>
					<td align="center">
						<div class="btn-group" role="group">
						  	{{ Form::open(array('method' => 'DELETE', 'route' => array('panel.admin.players.destroy', $user->id))) }}
						  		<a type="button" href="{{ route('panel.admin.players.edit', $user->id) }}" class="btn btn-default"> <i class="ion-gear-a"></i> Editar</a>
						  		<a type="button" class="destroy-record btn btn-default"> <i class="ion-trash-a"></i> Eliminar</a>
						  	{{ Form::close() }}
						</div>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	@if($users->count() == 0)
		<i>Aún sin registros</i>
	@endif
	{{ $users->links() }}
@stop