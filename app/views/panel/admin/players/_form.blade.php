<h4 class="title">Información del juagdor</h4>	
<hr />
<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			{{ Form::label('first_name', 'Nombre(s)', array('class' => 'input-required')) }}
			{{ Form::text('first_name', null, array('class' => 'form-control required ')) }}
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			{{ Form::label('last_name', 'Apellidos', array('class' => 'input-required')) }}
			{{ Form::text('last_name', null, array('class' => 'form-control required')) }}
		</div>
	</div>
</div>

<h4 class="title">Información de la cuenta</h4>	
<hr />
<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			{{ Form::label('email', 'Correo electrónico', array('class' => 'input-required')) }}
			{{ Form::text('email', null, array('class' => 'form-control required')) }}
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			@if(!$user->id)
				{{ Form::label('password', 'Contraseña', array('class' => '')) }}
			@else
				{{ Form::label('password', 'Actualizar contraseña', array('class' => '')) }}
			@endif
			{{ Form::password('password', array('class' => 'form-control')) }}
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			{{ Form::label('phone_number', 'Numero de teléfono', array('class' => 'input-required')) }}
			{{ Form::text('phone_number', $player->phone_number, array('class' => 'required form-control masked')) }}
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			{{ Form::label('birthday', 'Fecha de nacimiento', array('class' => 'input-required')) }} <small>Formato: 1990-12-31</small>
			{{ Form::text('birthday', $player->birthday, array('class' => 'required form-control datetime-date')) }}
		</div>
	</div>
</div>
<div class="form-group">
	{{ Form::label('birthplace', 'Lugar de nacimiento', array('class' => 'input-required')) }}
	{{ Form::text('birthplace', $player->birthplace, array('class' => 'required form-control')) }}
</div>

<div class="form-group">
	{{ Form::label('category_id', 'Categoria', array('class' => 'input-required')) }}
	{{ Form::select('category_id', $categories, $player->category_id, array('class' => 'form-control', 'placeholder' => '(664) 123-4567')) }}
</div>

<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			{{ Form::label('allergies', 'Alergias', array('class' => '')) }}
			{{ Form::textarea('allergies', $player->allergies, array('class' => 'form-control', 'rows' => '5')) }}
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			{{ Form::label('other_sports', 'Otros deportes', array('class' => '')) }}
			{{ Form::textarea('other_sports', $player->other_sports, array('class' => 'form-control', 'rows' => '5')) }}
		</div>
	</div>
</div>

