@extends('layout.admin')
@section('content')

{{ Form::model($field, array('route' => array('panel.admin.fields.store', $field->id), 'autocomplete' => 'off', 'id' => 'form-field', 'method' => 'POST')) }}

<div class="row">
	<div class="col-md-6">
		<h3 class="m0 title">Nuevo campo</h3>
	</div>
	<div class="col-md-6" align="right">
		{{  Form::submit('Guardar', array('class' => 'btn btn-success mr10')) }}
		<a href="{{ redirect_back() }}">Regresar</a>
	</div>
</div>
<hr />
<ol class="breadcrumb">
    <li><a href="{{ route('panel.admin.index') }}">Inicio</a></li>
    <li><a href="{{ route('panel.admin.fields.index') }}">Campos</a></li>
    <li class="active">Nuevo registro</li>
</ol>
@include('panel.admin.fields._form')

{{ Form::close() }}

<script type="text/javascript" src="{{ url('assets/javascripts/panel/admin/fields.js') }}"></script>

@stop