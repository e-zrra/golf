@extends('layout.admin')
@section('content')
	
	<div class="row">
		<div class="col-md-6">
			<h3 class="m0 title">Campos</h3>
		</div>
		<div class="col-md-6" align="right"> 
			<a href="{{ route('panel.admin.fields.create') }}" class="btn btn-default-success">
				<i class="ion-plus-round"></i> Agregar nuevo campo
			</a>
		</div>
	</div>
	<hr/>
	<ol class="breadcrumb">
	    <li><a href="{{ route('panel.admin.index') }}">Inicio</a></li>
	    <li>Campos</li>
	</ol>
	<table class="table table-bordered">
		<thead>
			<th>
				Nombre
			</th>
			<th>
				Lugar
			</th>
			<th width="200px">
			</th>
		</thead>
		<tbody>
			@foreach($fields as $field)
				<tr>
					<td>
						{{ $field->name }}
					</td>
					<td>
						{{ $field->place }}
					</td>
					<td align="center">
						<div class="btn-group" role="group">
						  	{{ Form::open(array('method' => 'DELETE', 'route' => array('panel.admin.fields.destroy', $field->id))) }}
						  		<a type="button" href="{{ route('panel.admin.fields.edit', $field->id) }}" class="btn btn-default"> <i class="ion-gear-a"></i> Editar</a>
						  		<a type="button" class="destroy-record btn btn-default"> <i class="ion-trash-a"></i> Eliminar</a>
						  	{{ Form::close() }}
						</div>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	@if($fields->count() == 0)
		<i>Aún sin registros</i>
	@endif
	{{ $fields->links() }}

@stop