
<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			{{ Form::label('name', 'Nombre', array('class' => 'input-required')) }}
			{{ Form::text('name', null, array('class' => 'form-control required ')) }}
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			{{ Form::label('place', 'Lugar', array('class' => 'input-required')) }}
			{{ Form::text('place', null, array('class' => 'form-control required', 'placeholder' => 'Tijuana Baja California')) }}
		</div>
	</div>
</div>
<div class="form-group">
	{{ Form::label('description', 'Descripción', array('class' => '')) }}
	{{ Form::textarea('description', null, array('class' => 'form-control')) }}
</div>

