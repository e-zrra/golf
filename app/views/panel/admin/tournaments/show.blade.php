@extends('layout.admin')
@section('content')

<div class="row">
    <div class="col-md-6">
        <h3 class="m0 title">Torneo: <b class="color-black">{{ $tournament->name }}</b></h3>
    </div>
    <div class="col-md-6" align="right">
        <a href="{{ route('panel.admin.tournaments.edit', $tournament->id) }}" class="btn btn-default mr10"> <i class="ion-gear-a"></i> Editar</a>
        <a href="{{ redirect_back() }}">Regresar</a>
    </div>
</div>
<hr />
<ol class="breadcrumb">
    <li><a href="{{ route('panel.admin.index') }}">Inicio</a></li>
    <li><a href="{{ route('panel.admin.tournaments.index') }}">Torneos</a></li>
    <li class="active">Detalles de registro</li>
</ol>

<div>
    <h3 class="title">Detalles del torneo</h3>
    <hr />
    <div class="well">
        <div>
            <div class="form-group row">
                <label class="col-sm-1 control-label">Nombre:</label>
                <div class="col-sm-3 form-group">
                    {{ $tournament->name }}
                </div>
                <label class="col-sm-1 control-label">Inicia:</label>
                <div class="col-sm-3">
                    {{ format_date($tournament->start_date) }}
                </div>
                <label class="col-sm-1 control-label">Finaliza:</label>
                <div class="col-sm-3">
                    {{ format_date($tournament->finish_date) }}
                </div>
            </div>
        </div>
        <div class="row">
            <div>
                <label class="col-sm-2 control-label">Descripción:</label>
                <div class="col-sm-4">
                    {{ ($tournament->description) ? $tournament->description : '---' }}
                </div>
            </div>
            <div>
                <label class="col-sm-2 control-label">Genero:</label>
                <div class="col-sm-4">
                    {{ ($tournament->gender) ? $tournament->gender->name : '---' }}
                </div>
            </div>
        </div>
    </div>
    <h3 class="title">Jugadores registrados</h3>
    <table class="table table-bordered">
        <thead>
            <th width="50px">
                #
            </th>
            <th>
                Nombre
            </th>
            <th width="250px"></th>
        </thead>
        <tbody>
            @foreach($users as $key => $data)
            <tr>
                @if($data->user)
                    <td align="center">
                        {{ $key + 1 }}
                    </td>
                    <td>
                        {{ $data->user->full_name }}
                    </td>
                    <td align="center">
                        <div class="btn-group" role="group">
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('panel.admin.user_tournaments.destroy', $data->id))) }}
                            <a type="button" href="{{ route('panel.admin.players.edit', $data->user->id) }}" class="btn btn-default"> <i class="ion-gear-a"></i> Ver detalles</a>
                            <a class="destroy-record btn btn-default"> <i class="ion-trash-a"></i> Eliminar</a>
                        {{ Form::close() }}
                        </div>
                    </td>
                @else
                    <i>No disponible</i>
                @endif
            </tr>
            @endforeach
        </tbody>
    </table>
    @if($users->count() == 0)
        <i>Aún sin registros</i>
    @endif
</div>
@stop