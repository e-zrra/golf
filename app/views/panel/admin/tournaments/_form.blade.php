<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			{{ Form::label('name', 'Nombre', array('class' => 'input-required')) }}
			{{ Form::text('name', null, array('class' => 'form-control required')) }}
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			{{ Form::label('gender_id', 'Genero', array('class' => 'input-required' )) }}
			{{ Form::select('gender_id', $genders, null, array('class' => 'form-control')) }}
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			{{ Form::label('start_date', 'Fecha de inicio', array('class' => 'input-required')) }}
			{{ Form::text('start_date', null, array('class' => 'form-control required datetime-date')) }}
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			{{ Form::label('finish_date', 'Fecha de finalización', array('class' => 'input-required', 'placeholder' => 'Tijuana Baja California')) }}
			{{ Form::text('finish_date', null, array('class' => 'form-control required datetime-date')) }}
		</div>
	</div>
</div>
<div class="form-group">
	@if($tournament->id)
		{{ Form::label('status', 'Estatus', array('class' => '')) }}
		<div class="radio-inline">
		  <label>
		    <input type="radio" name="status" value="0" @if($tournament->status == 0) {{ 'checked' }} @endif }}>
		    Activo
		  </label>
		</div>
		<div class="radio-inline">
		  <label>
		    <input type="radio" name="status" value="1" @if($tournament->status == 1) {{ 'checked' }} @endif }}>
		    Finalizado
		  </label>
		</div>
		<div class="radio-inline">
		  <label>
		    <input type="radio" name="status" value="2" @if($tournament->status == 2) {{ 'checked' }} @endif }}>
		    Cancelado
		  </label>
		</div>
	@endif
</div>

<div class="form-group">
	{{ Form::label('description', 'Descripción', array('class' => '')) }}
	{{ Form::textarea('description', null, array('class' => 'form-control')) }}
</div>

