@extends('layout.admin')
@section('content')
	
	<div class="row">
		<div class="col-md-6">
			<h3 class="m0 title">Torneos</h3>
		</div>
		<div class="col-md-6" align="right"> 
			<a href="{{ route('panel.admin.tournaments.create') }}" class="btn btn-default-success">
				<i class="ion-plus-round"></i> Agregar nuevo torneo
			</a>
		</div>
	</div>
	<hr/>
	<ol class="breadcrumb">
      	<li><a href="{{ route('panel.admin.index') }}">Inicio</a></li>
      	<li class="active">Torneos</li>
    </ol>
	<table class="table table-bordered">
		<thead>
			<th>
				Nombre
			</th>
			<th width="480px">
			</th>
		</thead>
		<tbody>
			@foreach($tournaments as $tournament)
				<tr>
					<td>
						{{ $tournament->name }}
					</td>
					<td align="center">
						<div class="btn-group" role="group">
						  	{{ Form::open(array('method' => 'DELETE', 'route' => array('panel.admin.tournaments.destroy', $tournament->id))) }}
						  		<a href="{{ route('panel.admin.tournaments.show', $tournament->id) }}" class="btn btn-default"> <i class=""></i> Ver detalles</a>
						  		<a href="{{ route('panel.admin.history.index', $tournament->id) }}" class="btn btn-default"> <i class=""></i> Historial</a>
						  		<a href="{{ route('panel.admin.stages.index', $tournament->id) }}" class="btn btn-default"><i class="ion-ios-browsers-outline"></i>  Etapas {{ $tournament->stage->count() }} </a>
						  		<a href="{{ route('panel.admin.tournaments.edit', $tournament->id) }}" class="btn btn-default"> <i class="ion-gear-a"></i> Editar</a>
						  		<a class="destroy-record btn btn-default"> <i class="ion-trash-a"></i> Eliminar</a>
						  	{{ Form::close() }}
						</div>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	@if($tournaments->count() == 0)
		<i>Aún sin registros</i>
	@endif
	{{ $tournaments->links() }}

@stop