@extends('layout.admin')
@section('content')

{{ Form::model($tournament, array('route' => array('panel.admin.tournaments.store', $tournament->id), 'autocomplete' => 'off', 'id' => 'form-tournament', 'method' => 'POST')) }}

<div class="row">
	<div class="col-md-6">
		<h3 class="m0 title">Nuevo torneo</h3>
	</div>
	<div class="col-md-6" align="right">
		{{  Form::submit('Guardar', array('class' => 'btn btn-success mr10')) }}
		<a href="{{ redirect_back() }}">Regresar</a>
	</div>
</div>
<hr />
<ol class="breadcrumb">
    <li><a href="{{ route('panel.admin.index') }}">Inicio</a></li>
    <li><a href="{{ route('panel.admin.tournaments.index') }}">Torneos</a></li>
    <li class="active">Nuevo registro</li>
</ol>
@include('panel.admin.tournaments._form')

{{ Form::close() }}

<script type="text/javascript" src="{{ url('assets/javascripts/panel/admin/tournaments.js') }}"></script>

@stop