@extends('layout.player')
@section('content')
	
	<h3 class="m0 title">Historial de torneos</h3>
	<hr/>
	@foreach($user_tournaments as $data)
		<div class="panel panel-default">
			<div class="panel-heading">
				@if($data->tournament)
					<b>Torneo:</b> {{ $data->tournament->name }}
				@endif
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-6">
						<h4>Etapas del torneo</h4>
					</div>
					<div class="col-md-6">
						<div align="right">
							@if($data->tournament)
								@if($data->tournament->stage)
									<span class="mr10">
										<i class="ion-ios-browsers-outline"></i> {{ count($data->tournament->stage) }} Etapas
									</span>
								@endif
							@endif
							<a href="#" class="btn btn-info"> <i class="ion-clipboard"></i> Puntaje general</a>
						</div>
					</div>
				</div>
				<div class="panel panel-default mt10">
					<ul class="list-group">
						@foreach($data->tournament->stage as $key => $stage)
							<?php $stage_user = $stage->stage_user(); ?>
					    	<li class="list-group-item">
					    		<div class="row">
						    		<div class="col-md-4">
						    			<b>{{ $key + 1 }}.-</b> {{ ($stage->description) ? str_limit($stage->description, 30, '...') : "Etapa " . ($key + 1)  }}
						    		</div>
						    		<div class="col-md-5">
						    			@if($stage_user)
						    				<b>Hoyo de inicio:</b> {{ $stage_user->start_hole }}
						    				<b>Hora de inicio:</b> {{ $stage_user->hour }}
						    			@else
						    				<i>Aún no disponible</i>
						    			@endif
						    		</div>
						    		<div class="col-md-3">
							    		@if($stage_user)
							    			<div align="right">
							    			@if($stage_user->status == 0)
							    				<a href="{{ route('panel.player.user_stage.stage', $stage->id) }}" class="mr10"> <i class="ion-flag"></i>
							    					<u>Puntaje</u>
								    				<span class="label label-primary"> <i class="ion-arrow-right-c"></i> Iniciado </span>
								    			</a>
							    			@elseif($stage_user->status == 1)
							    				<span class="label label-warning"> <i class="ion-checkmark-round"></i> Finalizado</span>
							    			@endif
							    			</div>
							    		@else
							    			<div align="right">
							    				<!-- <a href="{{ route('panel.player.user_stage.store', array($stage->id, $data->tournament_id)) }}" class="mr10"> <i class="ion-flag"></i> Iniciar etapa</a> -->
							    				<i class="label label-warning ion-shuffle"> No habilitada</i>
							    			</div>
							    		@endif
						    		</div>
					    		</div>
					    	</li>
					    @endforeach
				  	</ul>
				</div>
			</div>
		</div>
	@endforeach
	
	@if($user_tournaments->count() == 0)
		<i>Aún sin registros</i>
	@endif
@stop