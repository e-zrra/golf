@extends('layout.player')
@section('content')
	
	<div class="row">
		<div class="col-md-10">
			<h3 class="m0 title"><b>Puntaje de la etapa:</b> <span class="color-black">{{ $stage->description }}</span> </h3>
		</div>
		<div class="col-md-2">
			<a href="{{ redirect_back() }}">Regresar</a>
		</div>
	</div>
	<hr/>

	<h3>Hoyos</h3>
	<div class="row">
		@foreach($hole_scores as $key => $hole)
			@if(!($key % 9))
				<div class="row"></div>
			@endif
			<div class="col-md-1-5 pr0">
				<div class="form-group">
					<div align="center"><label>{{ $hole->hole }}</label></div>
					<input type="text" class="form-control"></input>
				</div>
			</div>

		@endforeach
	</div>

@stop