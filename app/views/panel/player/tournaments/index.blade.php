@extends('layout.player')
@section('content')
	
	<h3 class="m0 title">Torneos</h3>
	<hr/>
	<div class="row">
	@foreach($tournaments as $key => $tournament)
		@if(!($key % 1))
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4>{{ $tournament->name }}
					<span class="pull-right">
						@if($tournament->status == 1)
							<label class="label label-danger">Finalizado</label>
						@elseif($tournament->status == 2)
							<label class="label label-danger">Cancelado</label>
						@else
						@endif
					</span>
					</h4>
				</div>
				<div class="panel-body">
					
					<div class="form-group row">
						<label class="col-sm-4">Fechas:</label>
						<div class="col-sm-8">
							{{ format_date($tournament->date_start) }} <b>a</b> {{ format_date($tournament->date_finish) }}
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-4">Descripción:</label>
						<div class="col-sm-8">
						{{ ($tournament->description) ? str_limit($tournament->description, 100, '...') : '<i>Sin descripción</i>' }}
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-4">Genero:</label>
						<div class="col-sm-8">
							{{ ($tournament->gender) ? $tournament->gender->name : '<i>Sin descripción</i>' }}
						</div>
					</div>
					<div class="row">
						<label class="col-sm-4">Numero de etapas:</label>
						<div class="col-sm-8">
							{{ count($tournament->stage) }}
						</div>
					</div>
					<a class="pointer" role="button" data-toggle="collapse" href="#collapse_stages_{{ $tournament->id }}" aria-expanded="false" aria-controls="collapse_stages_{{ $tournament->id }}"> <i class="ion-ios-browsers-outline"></i> Ver etapas del torneo</a>
					
					<div class="collapse" id="collapse_stages_{{ $tournament->id }}">
					  <div class="well">
					    <h4 class="mt0">Etapas</h4>
					    <table class="table">
						<thead>
							<th width="10px">
								#
							</th>
							<th>
								Campo
							</th>
							<th>
								Fechas
							</th>
						</thead>
						<tbody>
							@foreach($tournament->stage as $key => $stage)
							<tr>
								<td>
									{{ $key + 1 }}
								</td>
								<td width="200px">
									{{ ($stage->field) ? $stage->field->name : '<i>No disponible</i>' }}
								</td>
								<td>
									{{ format_date($stage->start_date) }} <b>a</b> {{ format_date($stage->start_date) }}
								</td>
								<td>
									{{ $stage->description }}
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					@if(count($tournament->stage) == 0)
						<i>Aún sin registros</i>
					@endif
					  </div>
					</div>		

					@if($tournament->is_subscribed())
					<hr />
					<div align="center">
						<a>¡Ya te encuentras registrado!</a>
					</div>
					@endif
				</div>
				<div class="panel-footer" align="right">
					@if(!$tournament->is_subscribed() && $tournament->status == 0)
						<a href="{{ route('panel.player.tournaments.subscribe', $tournament->id) }}" class="btn btn-primary"> Registrarse </a>
					@else
						<div align="right">
							<a href="{{ route('panel.player.tournaments.subscribe', $tournament->id) }}" class="btn btn-default">Ver detalles</a>
						</div>
					@endif
				</div>
			</div>
		</div>
		@endif
	@endforeach
	</div>
	@if($tournaments->count() == 0)
		<i>Aún sin registros</i>
	@endif
@stop