@extends('layout.player')
@section('content')

	{{ Form::model($tournament, array('route' => ['panel.player.tournaments.subscribe_store', $tournament->id], 'autocomplete' => 'off', 'method' => 'POST', 'id' => 'form-player-subcribe')) }}
	<div class="row">
		<div class="col-md-8">
			<h3 class="m0 title">Registro del torneo <b class="color-black"> {{ $tournament->name }}</b></h3>
		</div>
		<div class="col-md-4" align="right"> 
			@if($user_tour)
				{{ Form::submit('Registrado', array('class' => 'btn btn-default mr10 disabled', 'disabled' => 'disabled')) }}
			@endif

			@if(!$user_tour && $tournament->status == 0)
				{{ Form::submit('Registrarse al torneo', array('class' => 'btn btn-success mr10')) }}
			@elseif($tournament->status == 1)
				<label class="text-danger mr10">Finalizado</label>
			@elseif($tournament->status == 2)
				<label class="text-danger mr10">Cancelado</label>
			@endif
			<a href="{{ redirect_back() }}">Regresar</a>
		</div>
	</div>
	<hr/>
	
	<h3 class="title">Detalles del torneo</h3>
	<br />
	<div class="form-group row">
		<label class="col-sm-4">Fecha de inicio:</label>
		<div class="col-sm-8">
			{{ format_date($tournament->start_date) }}
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-4">Fecha de finalización:</label>
		<div class="col-sm-8">
			{{ format_date($tournament->finish_date) }}
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-4">Descripción:</label>
		<div class="col-sm-8">
			{{ $tournament->description }}
		</div>
	</div>
	<div class="row">
		<label class="col-sm-4">Genero:</label>
		<div class="col-sm-8">
			{{ ($tournament->gender) ? $tournament->gender->name : '<i>Sin descripción</i>' }}
		</div>
	</div>
	{{ Form::close() }}
	<br />
	<h4 class="title">Etapas del torneo</h4>
	<hr />

	<table class="table">
		<thead>
			<th width="10px">
				#
			</th>
			<th>
				Campo
			</th>
			<th>
				Fechas
			</th>
		</thead>
		<tbody>
			@foreach($stages as $stage)
			<tr>
				<td>
					{{ $stage->id }}
				</td>
				<td width="200px">
					{{ ($stage->field) ? $stage->field->name : '<i>No disponible</i>' }}
				</td>
				<td>
					{{ format_date($stage->start_date) }} <b>a</b> {{ format_date($stage->start_date) }}
				</td>
				<td>
					{{ $stage->description }}
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	@if(count($stages) == 0)
		<i>Aún sin registros</i>
	@endif

	<script type="text/javascript" src="{{ url('assets/javascripts/panel/player/tournaments.js') }}"></script>

@stop