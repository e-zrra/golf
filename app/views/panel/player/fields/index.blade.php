@extends('layout.player')
@section('content')
	
	<h3 class="m0 title">Campos</h3>
	<hr/>
	<div class="row">
	@foreach($fields as $field)
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">
					{{ $field->name }}
				</div>
				<div class="panel-body">
					<div class="row">
						<label class="col-sm-3">Lugar:</label>
						<div class="col-sm-9">	
							{{ $field->place }}
						</div>
					</div>
					<div class="row">
						<label class="col-sm-3">Descripción:</label>
						<div class="col-md-9">
							{{ $field->description }}
						</div>
					</div>
				</div>
			</div>
		</div>
	@endforeach
	</div>
	@if($fields->count() == 0)
		<i>Aún sin registros</i>
	@endif

@stop