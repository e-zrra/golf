<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			{{ Form::label('first_name', 'Nombre(s)', array('class' => 'input-required')) }}
			{{ Form::text('first_name', null,array('class' => 'form-control required')) }}
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			{{ Form::label('last_name', 'Apellidos', array('class' => 'input-required')) }}
			{{ Form::text('last_name', null,array('class' => 'form-control required')) }}
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			{{ Form::label('phone_number', 'Teléfono', array('class' => 'input-required')) }}
			{{ Form::text('phone_number', null, array('class' => 'masked form-control required', 'placeholder' => '(664) 123-4567')) }}
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			{{ Form::label('email', 'Correo electrónico de tutor', array('class' => 'input-required')) }}
			{{ Form::text('email', null, array('class' => 'form-control required')) }}
		</div>
	</div>
</div>
