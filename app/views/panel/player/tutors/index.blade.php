@extends('layout.player')
@section('content')
	
	<div class="row">
		<div class="col-md-6">
			<h3 class="m0 title">Mis tutores</h3>
		</div>
		<div class="col-md-6" align="right"> 
			<a href="{{ route('panel.player.tutors.create') }}" class="btn btn-default-success"> <i class="ion-plus-round"></i> Agregar nuevo tutor</a>
		</div>
	</div>
	<hr />

	<div>
		<table class="table table-bordered">
			<thead>
				<th>
					Nombre
				</th>
				<th>
					Teléfono
				</th>
				<th width="200px">
				</th>
			</thead>
			<tbody>
				@foreach($tutors as $key => $tutor)
				<tr>
					<td>
						{{ $tutor->full_name() }}
					</td>
					<td>
						{{ $tutor->phone_number }}
					</td>
					<td align="center">
						<div class="btn-group" role="group">
						  	{{ Form::open(array('method' => 'DELETE', 'route' => array('panel.player.tutors.destroy', $tutor->id))) }}
						  		<a type="button" href="{{ route('panel.player.tutors.edit', $tutor->id) }}" class="btn btn-default"> <i class="ion-gear-a"></i> Editar</a>
						  		<a type="button" class="destroy-record btn btn-default"> <i class="ion-trash-a"></i> Eliminar</a>
						  	{{ Form::close() }}
						</div>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
@stop
