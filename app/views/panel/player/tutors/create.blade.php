@extends('layout.player')
@section('content')

{{ Form::model($tutor, array('route' => array('panel.player.tutors.store', $tutor->id), 'autocomplete' => 'off', 'id' => 'form-tutor', 'method' => 'POST')) }}

<div class="row">
	<div class="col-md-6">
		<h3 class="m0 title">Nuevo tutor</h3>
	</div>
	<div class="col-md-6" align="right">
		{{  Form::submit('Guardar', array('class' => 'btn btn-success mr10')) }}
		<a href="{{ redirect_back() }}">Regresar</a>
	</div>
</div>
<hr />
@include('panel.player.tutors._form')

{{ Form::close() }}

<script type="text/javascript" src="{{ url('assets/javascripts/panel/player/tutors.js') }}"></script>

@stop