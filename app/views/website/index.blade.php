@extends('layout.website')
@section('content')

<div class="hero hero-signin section-text"></div>
<div align="center">
	<h3>Sitio Web</h3>
	<div>
		<a class="" href="{{ route('website.index') }}"> Inicio </a>
	</div>
	@if(Auth::check())
		<a href="{{ route('session.user_panel') }}"> 
			Perfil <i class="fa fa-cog fa-sm"></i>
		</a>
	@else
		<div>
			<a href="{{ route('session.signup') }}">Registrarse</a> <br />
			<a href="{{ route('session.signin') }}">Iniciar sesion</a>
		</div>
	@endif
</div>

@stop