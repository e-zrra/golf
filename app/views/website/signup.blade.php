@extends('layout.website')
@section('content')
	<div class="hero hero-signup section-text">
    	<div class="container" align="right">
			<h2> Registros </h2>
			<h3>  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua.</h3>
			<a href="#" class="btn btn-white-border"> Registrarse aún torneo </a>
		</div>
   	</div>
	<div class="section">
		<div class="container">
			{{ Form::open(array('route' => 'session.signup_store', 'method' => 'POST', 'id' => 'form-signup-player', 'autocomplete' => 'on')) }}
			<h2 class="text-green">Registro</h2>
			<hr />
			<div class="row">
				<div class="col-md-6">
					<h4>Datos del jugador</h4>
					<div class="form-group">
						{{ Form::label('first_name', 'Nombre(s)', array('class' => 'input-required')) }}
						{{ Form::text('first_name', null, array('class' => 'form-control')) }}
					</div>

					<div class="form-group">
						{{ Form::label('last_name', 'Apellidos', array('class' => 'input-required')) }}
						{{ Form::text('last_name', null, array('class' => 'form-control')) }}
					</div>

					<div class="form-group">
						{{ Form::label('phone_number', 'Teléfono', array('class' => 'input-required')) }}
						{{ Form::text('phone_number', null, array('class' => 'form-control masked', 'placeholder' => '(664) 123-4567')) }}
					</div>

					<div class="form-group">
						{{ Form::label('category_id', 'Categoria', array('class' => 'input-required')) }}
						{{ Form::select('category_id', $categories, null, array('class' => 'form-control masked', 'placeholder' => '(664) 123-4567')) }}
					</div>

					<div class="form-group">
						{{ Form::label('birthday', 'Fecha de nacimiento', array('class' => 'input-required')) }} <small>Formato: 1995-12-31</small>
						{{ Form::text('birthday', null, array('class' => 'datetime-date form-control', 'placeholder' => 'yyyy-mm-dd')) }}
					</div>

					<div class="form-group">
						{{ Form::label('birthplace', 'Lugar de nacimiento', array('class' => 'input-required')) }}
						{{ Form::text('birthplace', null, array('class' => 'form-control', 'placeholder' => 'Tijuana, Baja California')) }}
					</div>

					<div class="form-group">
						{{ Form::label('allergies', 'Alergias') }}
						{{ Form::textarea('allergies', null, array('class' => 'form-control', 'rows' => '6')) }}
					</div>

					<div class="form-group">
						{{ Form::label('other_sports', 'Otros deportes') }}
						{{ Form::textarea('other_sports', null, array('class' => 'form-control', 'rows' => '6')) }}
					</div>
				</div>
				<div class="col-md-6">
					<h4>Datos de la cuenta</h4>
					<div class="form-group">
						{{ Form::label('email', 'Correo electrónico', array('class' => 'input-required')) }}
						{{ Form::text('email', null, array('class' => 'form-control')) }}
					</div>

					<div class="form-group">
						{{ Form::label('password', 'Contraseña', array('class' => 'input-required')) }}
						{{ Form::password('password', array('class' => 'form-control', 'id' => 'password')) }}
					</div>

					<div class="form-group">
						{{ Form::label('repeat_password', 'Repetir contraseña', array('class' => 'input-required')) }}
						{{ Form::password('repeat_password', array('class' => 'form-control')) }}
					</div><hr />
					<h4>Datos del tutor</h4>
					<div class="form-group">
						{{ Form::label('tutor_first_name', 'Nombre(s)', array('class' => 'input-required')) }}
						{{ Form::text('tutor_first_name', null,array('class' => 'form-control')) }}
					</div>
					<div class="form-group">
						{{ Form::label('tutor_last_name', 'Apellidos', array('class' => 'input-required')) }}
						{{ Form::text('tutor_last_name', null,array('class' => 'form-control')) }}
					</div>

					<div class="form-group">
						{{ Form::label('tutor_phone_number', 'Teléfono', array('class' => 'input-required')) }}
						{{ Form::text('tutor_phone_number', null, array('class' => 'masked form-control', 'placeholder' => '(664) 123-4567')) }}
					</div>
					<div class="form-group">
						{{ Form::label('tutor_email', 'Correo electrónico de tutor', array('class' => 'input-required')) }}
						{{ Form::text('tutor_email', null, array('class' => 'form-control')) }}
					</div>
				</div>
			</div>
			<div align="right">
				<input type="submit" class="btn btn-success" value="Registrarse" />
			</div>
			{{ Form::close() }}
		</div>
	</div>
	<script type="text/javascript" src="{{ url('assets/javascripts/website/signup.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/javascripts/website/helpers.js') }}"></script>
@stop