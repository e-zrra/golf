@extends('layout.website')
@section('content')
<div class="hero hero-signin section-text"></div>
<div class="mt40 container login-container">
	@if(Auth::check())
		<a href="{{ route('session.user_panel') }}"> 
			Perfil <i class="fa fa-cog fa-sm"></i>
		</a>
	@else
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
			{{ Form::open(array('route' => 'session.signin_store', 'method' => 'POST', 'id' => 'form-signin', 'autocomplete' => 'on')) }}
				<h3>Iniciar sesión</h3>
				<hr />
				<div class="form-group">
					{{ Form::label('email', 'Correo electrónico') }}
					{{ Form::text('email', '', array('class' => 'form-control')) }}
				</div>
				<div class="form-group">
					{{ Form::label('password', 'Contraseña') }}
					{{ Form::password('password', array('class' => 'form-control')) }}
				</div>
				<!-- <div class="form-group" align="right">
					<a>Recuperar contraseña</a>
				</div> -->
				<div class="form-group" align="right">
					<input type="submit" value="Accesar" class="btn btn-success" />
				</div>
			{{ Form::close() }}
			</div>
		</div>
	@endif
</div>
<script type="text/javascript" src="{{ url('assets/javascripts/website/signin.js') }}"></script>

@stop