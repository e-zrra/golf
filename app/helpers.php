<?php
use Carbon\Carbon;
use Jenssegers\Date\Date;

function format_date ($date) {

	$date = new Date($date);

	Date::setLocale('es'); //Lang::getLocale()

	return "{$date->format('j')} de {$date->format('F')} de {$date->format('Y')}";
}

function broadcaster_type_bg ($broadcaster_type_id) {

	$bg_img = "locutores.jpg";

	if (Input::get('broadcaster_type_id')) {

		$type = Input::get('broadcaster_type_id');
		
		switch ($type) {
			case 2:
				$bg_img = "estudio_de_grabacion.jpg";
				break;
			case 3:
				$bg_img = "produccionpost.jpg";
				break;
			case 4:
				$bg_img = "junglistas.jpg";
				break;
		}
	}
	return $bg_img;
}

function generate_nick ($word)
{
	$utf 	= array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ');
	$words 	= array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o');
	$word 	= str_replace($utf, $words, $word);
	$word 	= preg_replace('/[^\da-z ]/i', '-', $word);
	$word 	= str_replace(' ', '-', $word);
	$word 	= str_replace('_', '-', $word);
	$word 	= strtolower($word);
	return $word;
}

function convert_youtube($string) {
    return preg_replace(
        "/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i",
        "<iframe width='100%' frameborder='0' height='300' src=\"//www.youtube.com/embed/$2\" allowfullscreen></iframe>",
        $string
    );
}

function convert_vimeo ($string) {

	return preg_replace('#https?://(www\.)?vimeo\.com/(\d+)#',
     	'<iframe width="100%" height="300" class="videoFrame" src="//player.vimeo.com/video/$2" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>',
     	$string);
}

function convert_money ($int) {

	return number_format(($int/100), 2);
}

function get_months()
{
	return array(
		'01' => 'Enero', 
		'02' => 'Febrero', 
		'03' => 'Marzo', 
		'04' => 'Abril', 
		'05' => 'Mayo', 
		'06' => 'Junio', 
		'07' => 'Julio', 
		'08' => 'Agosto', 
		'09' => 'Septiembre', 
		'10' => 'Octubre', 
		'11' => 'Noviembre', 
		'12' => 'Diciembre'
	);
}

function is_route_welcome_index () {

	$url = '';

	if (Route::getCurrentRoute()->getPath() != '/') {
		
		$url = route('website.index');
	}

	return $url;
}

function nav_option_active ($module)
{
	$path = Request::segment(3);

	if (in_array($path, $module)) {
	
		return 'active';
	}
}

function redirect_back() {

	if (isset($_SERVER['HTTP_REFERER']) === true) {

		$url = URL::previous();
		
	} else {

		if (Auth::check()) {
			
			/* $user = Auth::user();

			if (!$user->role_user()) {
				
				return route('website.index');	
			}

			$url = route($user->role_user().'.panel.index'); */ 
		}

		$url = route('website.index');
	}

	return $url;
}

function casting_broadcaster_count () {

	$user = Auth::user();

	$count = 0;

	if ($user->broadcaster) {
		
		$count = Casting::where('user_id', $user->id)->whereNull('updated_at')->count();
	}

	return $count;
}

function new_contact_messages_count () {

	$user = Auth::user();

	$count = 0;

	if ($user->type_id == 1) {
		
		$count =  ContactMessage::where('viewed', 0)->count();
	}

	return $count;
}

function img_tag($src, $default, $folder, $alt = null, $width = null, $height = null, $class = null)
{
	if (!$src) $src = $default;

	$uri = "$folder/" . $src;

	$format = '<img src="%s" alt="%s" width="%s" height="%s" class="%s" />';

    return sprintf($format, url($uri), $alt, $width, $height, $class);
}

function update_file_in_public($data, $folder)
{
	try {

		$destination_path = public_path($folder);

		if (!File::isWritable($destination_path))
		{
		    $result = File::makeDirectory($destination_path);
		}

		$file_name = $data->getClientOriginalName();
        
        $data->move($destination_path, $file_name);
		
	} catch (Exception $e) {

		throw new Exception('Error con archivo o folder');

		$file_name = null;

		Log::error($e);
		
	}

	return $file_name;
}

function delete_file_in_public ($path)
{
	try {

		if (File::isWritable($path)) {

			File::delete($path);
		}

	} catch (Exception $e) {

		throw new Exception('No se puedo eliminar el archivo');

		Log::error($e);
		
		return false;
	}

	return true;
	
}