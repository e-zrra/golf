<?php

class AdminStageController extends \BaseController {

	public function index ($tournament_id)
	{
		$stages = Stage::where('tournament_id', $tournament_id)->orderBy('number', 'ASC')->paginate(10);

		$tournament = Tournament::find($tournament_id);

		return View::make('panel.admin.stages.index', compact('stages', 'tournament'));
	}

	public function create($tournament_id)
	{
		$stage = new Stage;

		$fields = Field::lists('name', 'id');

		$tournament = Tournament::find($tournament_id);

		return View::make('panel.admin.stages.create', compact('stage', 'tournament', 'fields'));
	}

	public function store($tournament_id)
	{
		if (!Request::ajax()) return Redirect::back();

		$json = array('title' => 'Agregado',
					  'text' => 'La etapa fue agregada correctamente',
					  'success' => true,
					  'redirect' => null);

		$data = Input::except('_token');

		try {

			DB::beginTransaction();

			$validator = Validator::make($data, Stage::$rules_validate, Stage::$rules_messages);

			if ($validator->fails()) throw new Exception($validator->messages());

			$stage = new Stage($data);

			$stage->tournament_id = $tournament_id;

			$stage->save();

			$json['redirect'] = route('panel.admin.stages.index', $tournament_id);

			DB::commit();	
			
		} catch (Exception $e) {

			DB::rollBack();

			$json['success'] = false;

			$json['title'] = 'Error:';

			Log::error($e);

			if ($validator->fails()) {

				$errors = $validator->messages()->all();

				$json['text']  = '';

				foreach ($errors as $error) {

			        $json['text'] .= $error.' <br />';

			    }

			} else {

				$json['text'] = $e->getMessage();
			}
			
		}

		return Response::json($json);
	}

	public function edit($tournament_id, $id)
	{
		$stage = Stage::find($id);

		$fields = Field::lists('name', 'id');

		$tournament = Tournament::find($tournament_id);

		return View::make('panel.admin.stages.edit', compact('stage', 'tournament', 'fields'));	
	}

	public function update($id)
	{
		if (!Request::ajax()) return Redirect::back();

		$json = array('title' => 'Actualizado', 'text' => 'Información del usuario actualizada.','success' => true, 'redirect' => null, 'show_button' => true);

		$data = Input::except('_token');

		try {

			DB::beginTransaction();

			$validator = Validator::make($data, Stage::$rules_validate, Stage::$rules_messages);

			if ($validator->fails()) throw new Exception($validator->messages());

			$stage = Stage::find($id);

			$stage->fill($data);

			$stage->save();

			// $json['redirect'] = route('panel.admin.stages.index', $stage->tournament_id);

			DB::commit();	
			
		} catch (Exception $e) {

			DB::rollBack();

			$json['success'] = false;

			$json['title'] = 'Error:';

			Log::error($e);

			if ($validator->fails()) {

				$errors = $validator->messages()->all();

				$json['text']  = '';

				foreach ($errors as $error) {

			        $json['text'] .= $error.' <br />';

			    }

			} else {

				$json['text'] = $e->getMessage();
			}
			
		}

		return Response::json($json);
	}

	public function destroy($id)
	{
		try {
			
			Stage::destroy($id);

			Session::flash('message', 'Registro fue eliminado.');

			Session::flash('alert-class', 'alert-success'); 

		} catch (Exception $e) {
			
			Session::flash('message', 'Registro no se pudo eliminar.');

			Session::flash('alert-class', 'alert-danger'); 
		}

		return  Redirect::back();
	}

}