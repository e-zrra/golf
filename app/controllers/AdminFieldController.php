<?php

class AdminFieldController extends \BaseController {

	
	public function index()
	{
		$fields = Field::orderBy('id', 'DESC')->paginate();

		return View::make('panel.admin.fields.index', compact('fields'));
	}

	public function create()
	{
		$field = new Field;

		return View::make('panel.admin.fields.create', compact('field'));
	}

	public function store()
	{
		if (!Request::ajax()) return Redirect::back();

		$json = array('title' => 'Creado correctamente',
					  'text' => 'El registro fue creado correctamente.',
					  'success' => true,
					  'redirect' => null);

		$data = Input::except('_token');

		try {

			DB::beginTransaction();

			$validator = Validator::make($data, Field::$rules_validate, Field::$rules_messages);

			if ($validator->fails()) throw new Exception($validator->messages());

			$field = new Field($data);

			$field->save();

			$json['redirect'] = route('panel.admin.fields.index');

			DB::commit();	
			
		} catch (Exception $e) {

			DB::rollBack();

			$json['success'] = false;

			$json['title'] = 'Error:';

			Log::error($e);

			if ($validator->fails()) {

				$errors = $validator->messages()->all();

				$json['text']  = '';

				foreach ($errors as $error) {

			        $json['text'] .= $error.' <br />';

			    }

			} else {

				$json['text'] = $e->getMessage();
			}
			
		}

		return Response::json($json);
	}

	public function edit($id)
	{
		$field = Field::find($id);

		return View::make('panel.admin.fields.edit', compact('field'));
	}

	public function update($id)
	{
		if (!Request::ajax()) return Redirect::back();

		$json = array('title' => 'Actualizado',
					  'success' => true,
					  'text' => 'El registro fue actualizado correctamente.',
					  'redirect' => null,
					  'show_button' => true);

		$data = Input::except('_token');

		try {

			DB::beginTransaction();

			$validator = Validator::make($data, Field::$rules_validate, Field::$rules_messages);

			if ($validator->fails()) throw new Exception($validator->messages());

			$field = Field::find($id);

			$field->fill($data);

			$field->save();

			DB::commit();	
			
		} catch (Exception $e) {

			DB::rollBack();

			$json['success'] = false;

			$json['title'] = 'Error:';

			Log::error($e);

			if ($validator->fails()) {

				$errors = $validator->messages()->all();

				$json['text']  = '';

				foreach ($errors as $error) {

			        $json['text'] .= $error.' <br />';

			    }

			} else {

				$json['text'] = $e->getMessage();
			}
			
		}

		return Response::json($json);
	}

	public function destroy($id)
	{
		try {
			
			Field::destroy($id);

			Session::flash('message', 'Registro fue eliminado.');

			Session::flash('alert-class', 'alert-success'); 

		} catch (Exception $e) {
			
			Session::flash('message', 'Registro no se pudo eliminar.');

			Session::flash('alert-class', 'alert-danger'); 
		}

		return  Redirect::back();
	}

}