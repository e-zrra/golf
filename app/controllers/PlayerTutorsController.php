
<?php

class PlayerTutorsController extends BaseController {

	public $player;

	public $user;

	public function __construct () {

		$this->user 	= Auth::user();

		$this->player = Player::where('user_id', $this->user->id)->first();

	}

	public function index ()
	{
		$tutors = Tutor::where('player_id', $this->player->id)->paginate();

		return View::make('panel.player.tutors.index', compact('tutors'));
	}

	public function create () {

		$tutor = new Tutor;

		return View::make('panel.player.tutors.create', compact('tutor'));
	}

	public function store () {

		if (!Request::ajax()) return Redirect::back();

		$json = array('title' => 'Creado', 
					  'text' => 'El registro fue creado correctamente.',
					  'success' => true,
					  'redirect' => null);

		$data = Input::except('_token');

		try {

			DB::beginTransaction();

			$validator = Validator::make($data, Tutor::$rules_validate, Tutor::$rules_messages);

			if ($validator->fails()) throw new Exception($validator->messages());

			$tutor = new Tutor($data);

			$tutor->player_id = $this->player->id;

			$tutor->save();

			$json['redirect'] = route('panel.player.tutors.index');

			DB::commit();	
			
		} catch (Exception $e) {

			DB::rollBack();

			$json['success'] = false;

			$json['title'] = 'Error:';

			Log::error($e);

			if ($validator->fails()) {

				$errors = $validator->messages()->all();

				$json['text']  = '';

				foreach ($errors as $error) {

			        $json['text'] .= $error.' <br />';

			    }

			} else {

				$json['text'] = $e->getMessage();
			}
			
		}

		return Response::json($json);
	}

	public function edit ($id) {

		$tutor = Tutor::find($id);

		return View::make('panel.player.tutors.edit', compact('tutor'));

	}

	public function update ($id) {

		if (!Request::ajax()) return Redirect::back();

		$json = array('title' => 'Actualizado', 
					  'text' => 'El registro fue actualizado correctamente.',
					  'success' => true, 
					  'redirect' => null, 
					  'show_button' => true);

		$data = Input::except('_token');

		try {

			DB::beginTransaction();

			$validator = Validator::make($data, Tutor::$rules_validate, Tutor::$rules_messages);

			if ($validator->fails()) throw new Exception($validator->messages());

			$tutor = Tutor::find($id);

			$tutor->fill($data);

			$tutor->save();

			DB::commit();	
			
		} catch (Exception $e) {

			DB::rollBack();

			$json['success'] = false;

			$json['title'] = 'Error:';

			Log::error($e);

			if ($validator->fails()) {

				$errors = $validator->messages()->all();

				$json['text']  = '';

				foreach ($errors as $error) {

			        $json['text'] .= $error.' <br />';

			    }

			} else {

				$json['text'] = $e->getMessage();
			}
			
		}

		return Response::json($json);
	}

	public function destroy ($id) {

		try {

			Tutor::destroy($id);

			Session::flash('message', 'Registro fue eliminado.');

			Session::flash('alert-class', 'alert-success'); 

		} catch (Exception $e) {
			
			Session::flash('message', 'Registro no se pudo eliminar.');

			Session::flash('alert-class', 'alert-danger'); 
		}

		return  Redirect::back();

	} 

}
