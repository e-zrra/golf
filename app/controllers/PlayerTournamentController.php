<?php

class PlayerTournamentController extends \BaseController {

	
	public function index()
	{
		$tournaments = Tournament::orderBy('id', 'DESC')->paginate(10);

		return View::make('panel.player.tournaments.index', compact('tournaments'));
	}

    public function subscribe ($tournament_id) {

        $tournament = Tournament::find($tournament_id);

        $user_tour = UserTournament::where('tournament_id', $tournament_id)->where('user_id', Auth::user()->id)->first();

        $stages = Stage::where('tournament_id', $tournament->id)->get();

        return View::make('panel.player.tournaments.subscribe', compact('tournament', 'user_tour', 'stages'));
    }

    public function subscribe_store ($tournament_id) {

        if (!Request::ajax()) return Redirect::back();

        $json = array('title' => 'Registrado',
                      'text' => 'Te has registrado al torneo',
                      'success' => true,
                      'show_button' =>  true,
                      'redirect' => null);

        $data = Input::except('_token');

        try {

            DB::beginTransaction();

            $user_tour = new UserTournament([
                'tournament_id' => $tournament_id,
                'user_id' => Auth::user()->id,
                'status' => 0
            ]);

            $user_tour->save();

            // $json['redirect'] = route('panel.admin.stages.index', $tournament_id);

            DB::commit();   
            
        } catch (Exception $e) {

            DB::rollBack();

            $json['success'] = false;

            $json['title'] = 'Error:';

            Log::error($e);

            $json['text'] = $e->getMessage();
            
        }

        return Response::json($json);
    }

    public function subscribe_destroy($id)
    {
        try {
            
            UserTournament::destroy($id);

            Session::flash('message', 'Registro fue eliminado.');

            Session::flash('alert-class', 'alert-success');

        } catch (Exception $e) {
            
            Session::flash('message', 'Registro no se pudo eliminar.');

            Session::flash('alert-class', 'alert-danger'); 
        }

        return  Redirect::back();
    }

}