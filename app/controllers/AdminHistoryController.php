<?php

class AdminHistoryController extends \BaseController {

	
	public function index($tournament_id)
	{
		$stages = Stage::where('tournament_id', $tournament_id)->get();

		$user_tournaments = UserTournament::where('tournament_id', $tournament_id)->get();

		return View::make('panel.admin.history.index', compact('stages', 'user_tournaments'));
	}
}