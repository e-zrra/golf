<?php

class PlayerUserStageController extends BaseController {


	public $player;

	public $user;

	public function __construct () {

		$this->user 	= Auth::user();

		$this->player = Player::where('user_id', $this->user->id)->first();

	}

	public function edit ($id) {


	}

	public function store ($stage_id, $tournament_id)
	{

		/* $user_stage = new UserStage;

		$user_stage->stage_id = $stage_id;

		$user_stage->tournament_id = $tournament_id;

		$user_stage->user_id = $this->user->id;

		$user_stage->save();

		$hole = new StageHoleScore;

		$hole->user_stage_id = $user_stage->id;

		$hole->user_id = $this->user->id;

		$hole->stage_id = $stage_id;

		$hole->save();

		return Redirect::back(); */
	}

	public function stage ($stage_id) {

		$stage = Stage::find($stage_id);

		$hole_scores = StageHoleScore::where('stage_id', $stage_id)->get();

		return View::make('panel.player.history.stage', compact('stage', 'hole_scores'));
	}

}