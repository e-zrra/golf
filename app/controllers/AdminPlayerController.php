<?php

class AdminPlayerController extends \BaseController {

	public function index()
	{

		$users = User::where('user_type_id', 2)->paginate();

		return View::make('panel.admin.players.index', compact('users'));
	}

	public function create () {

		$categories = Category::lists('name', 'id');

		$user = new User();

		$player = new Player;

		return View::make('panel.admin.players.create', compact('user', 'player', 'categories'));
	}

	public function store () {

		if (!Request::ajax()) return Redirect::back();

		$json = array('title' => 'Creado',
					  'text' => 'El usuario fue creado correctamente',
					  'success' => true,
					  'redirect' => null);

		$data = Input::except('_token');

		try {

			DB::beginTransaction();

			$validator = Validator::make($data, User::$create_validate, User::$create_messages);

			if ($validator->fails()) throw new Exception($validator->messages());

			User::unique_user($data['email'], 0);

			$user = new User($data);

			$user->full_name = $data['first_name'] . ' ' . $data['last_name'];

			$user->user_type_id = 2;

			$user->password = Hash::make($data['password']);

			$user->save();

			$player = new Player($data);

			$player->user_id = $user->id;

			$player->save();

			$user = User::find($user->id);

			$user->player_id = $player->id;

			$user->save();

			$json['redirect'] = route('panel.admin.players.index');

			DB::commit();	
			
		} catch (Exception $e) {

			DB::rollBack();

			$json['success'] = false;

			$json['title'] = 'Error:';

			Log::error($e);

			if ($validator->fails()) {

				$errors = $validator->messages()->all();

				$json['text']  = '';

				foreach ($errors as $error) {

			        $json['text'] .= $error.' <br />';

			    }

			} else {

				$json['text'] = $e->getMessage();
			}
			
		}

		return Response::json($json);

	}

	public function edit ($id) {

		$categories = Category::lists('name', 'id');

		$user = User::find($id);

		$player = Player::where('user_id', $user->id)->first();

		if (!$user || !$player) {
			
			Session::flash('message', 'Registro no encontrado.');

			Session::flash('alert-class', 'alert-danger'); 

			return  Redirect::back();
		}

		return View::make('panel.admin.players.edit', compact('user', 'player', 'categories'));
	}

	public function update ($id) {

		if (!Request::ajax()) return Redirect::back();

		$json = array('title' => 'Actualizado', 'text' => 'Información del usuario actualizada.','success' => true, 'redirect' => null, 'show_button' => true);

		$data = Input::except('_token');

		try {

			DB::beginTransaction();

			$validator = Validator::make($data, User::$edit_validate, User::$edit_messages);

			if ($validator->fails()) throw new Exception($validator->messages());

			$user = User::find($id);

			User::unique_user($data['email'], $user->id);

			if ($data['password'] != '') $user->password = Hash::make($data['password']);

			$user->fill($data);

			$user->save();

			$player = Player::where('user_id', $user->id)->first();

			$player = Player::find($player->id);

			$player->fill($data);

			$player->save();

			DB::commit();	
			
		} catch (Exception $e) {

			DB::rollBack();

			$json['success'] = false;

			$json['title'] = 'Error:';

			Log::error($e);

			if ($validator->fails()) {

				$errors = $validator->messages()->all();

				$json['text']  = '';

				foreach ($errors as $error) {

			        $json['text'] .= $error.' <br />';

			    }

			} else {

				$json['text'] = $e->getMessage();
			}
			
		}

		return Response::json($json);
	}

	public function destroy ($id) {

		try {

			$user = User::find($id);
			
			User::destroy($id);

			$player = Player::where('user_id', $user->id)->first();

			Player::destroy($player->id);

			Session::flash('message', 'Registro fue eliminado.');

			Session::flash('alert-class', 'alert-success'); 

		} catch (Exception $e) {
			
			Session::flash('message', 'Registro no se pudo eliminar.');

			Session::flash('alert-class', 'alert-danger'); 
		}

		return  Redirect::back();
	}

}