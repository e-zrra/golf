<?php

class PlayersPanelController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /players
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('panel.player.index');
	}

}