<?php

class PlayerFieldController extends \BaseController {

	public function index()
	{
		$fields = Field::orderBy('name', 'ASC')->paginate(10);

		return View::make('panel.player.fields.index', compact('fields'));
	}
}