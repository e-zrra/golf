<?php

class AdminTournamentController extends \BaseController {

	public function index()
	{
		$tournaments = Tournament::orderBy('id', 'DESC')->paginate(10);

		return View::make('panel.admin.tournaments.index', compact('tournaments'));
	}

	public function create()
	{
		$tournament = new Tournament;

		$genders = Gender::lists('name', 'id');

		return View::make('panel.admin.tournaments.create', compact('tournament', 'genders'));
	}

	public function show ($id)
	{
		$tournament = Tournament::find($id);

		$users = UserTournament::where('tournament_id', $id)->get();
		
		return View::make('panel.admin.tournaments.show', compact('tournament', 'users'));
	}

	public function store()
	{
		if (!Request::ajax()) return Redirect::back();

		$json = array('title' => 'Creado', 
					  'text' => 'El registro fue creado correctamente.',
					  'success' => true,
					  'redirect' => null);

		$data = Input::except('_token');

		try {

			DB::beginTransaction();

			$validator = Validator::make($data, Tournament::$rules_validate, Tournament::$rules_messages);

			if ($validator->fails()) throw new Exception($validator->messages());

			$tournament = new Tournament($data);

			$tournament->save();

			$json['redirect'] = route('panel.admin.tournaments.edit', $tournament->id);

			DB::commit();	
			
		} catch (Exception $e) {

			DB::rollBack();

			$json['success'] = false;

			$json['title'] = 'Error:';

			Log::error($e);

			if ($validator->fails()) {

				$errors = $validator->messages()->all();

				$json['text']  = '';

				foreach ($errors as $error) {

			        $json['text'] .= $error.' <br />';

			    }

			} else {

				$json['text'] = $e->getMessage();
			}
			
		}

		return Response::json($json);
	}

	public function edit($id)
	{
		$tournament = Tournament::find($id);

		$genders = Gender::lists('name', 'id');

		return View::make('panel.admin.tournaments.edit', compact('tournament', 'genders'));
	}

	public function update($id)
	{
		if (!Request::ajax()) return Redirect::back();

		$json = array('title' => 'Actualizado',
					  'success' => true,
					  'text' => 'El registro fue actualizado correctamente.',
					  'redirect' => null,
					  'show_button' => true);

		$data = Input::except('_token');

		try {

			DB::beginTransaction();

			$validator = Validator::make($data, Tournament::$rules_validate, Tournament::$rules_messages);

			if ($validator->fails()) throw new Exception($validator->messages());

			$tournament = Tournament::find($id);

			$tournament->fill($data);

			$tournament->save();

			DB::commit();	
			
		} catch (Exception $e) {

			DB::rollBack();

			$json['success'] = false;

			$json['title'] = 'Error:';

			Log::error($e);

			if ($validator->fails()) {

				$errors = $validator->messages()->all();

				$json['text']  = '';

				foreach ($errors as $error) {

			        $json['text'] .= $error.' <br />';

			    }

			} else {

				$json['text'] = $e->getMessage();
			}
			
		}

		return Response::json($json);
	}

	public function destroy($id)
	{
		try {
			
			Tournament::destroy($id);

			Session::flash('message', 'Registro fue eliminado.');

			Session::flash('alert-class', 'alert-success');

		} catch (Exception $e) {
			
			Session::flash('message', 'Registro no se pudo eliminar.');

			Session::flash('alert-class', 'alert-danger'); 
		}

		return  Redirect::back();
	}

}