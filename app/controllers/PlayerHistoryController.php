<?php

class PlayerHistoryController extends BaseController {


	public $player;

	public $user;

	public function __construct () {

		$this->user 	= Auth::user();

		$this->player	= Player::where('user_id', $this->user->id)->first();

	}

	public function index ()
	{

		$user_tournaments = UserTournament::where('user_id', $this->user->id)->orderBy('id', 'DESC')->paginate(10);

		return View::make('panel.player.history.index', compact('user_tournaments'));
	}

}