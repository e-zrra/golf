<?php

class SessionController extends BaseController {

	public function signin () {

		return View::make('website.signin');
	}

	public function user_panel () {

		$user = Auth::user();

		if ($user->user_type_id == 2) {
			
			return Redirect::to(route('panel.player.index'));
		}

		else if ($user->user_type_id == 1) {

			return Redirect::to(route('panel.admin.index'));
			
		} else  {

			Auth::logout();

			Session::flush();

			return Redirect::to(route('session.signin'));
		}
	}

	/**
	 * Authenticate a user credentials
	 * POST /login
	 *
	 * @return Response
	 */
	public function signin_store()
	{
		if (!Request::ajax()) return Redirect::back();
		
		$json = array('success' => true, 'title' => 'Iniciando sesión', 'text' => 'Procesando ...', 'redirect' => null);

		try {

			$email 		= HTML::entities(Input::get('email'));

			$password 	= HTML::entities(Input::get('password'));

			$credentials = array('email' => $email,'password' => $password);

	        if (Auth::attempt($credentials)) {

	        	$user = Auth::user();

	        	if ($user->user_type_id == 2) {
	        		
	        		$json['redirect'] = route('panel.player.index');
	        	}
	        	
	        	else if ($user->user_type_id == 1) {

					$json['redirect'] = route('panel.admin.index');
				}

				else {

	        		Auth::logout();
		    
		    		Session::flush();

	        		throw new Exception("Error: Processing Request");
	        		
	        	}

	        } else {

	        	throw new Exception("Acceso denegado: Correo electrónico o contraseña incorrecta");
	        }

		} catch (Exception $e) {

			$json['success'] = false;

			$json['title'] = 'Error.';

			$json['text'] = $e->getMessage();

			Log::error($e);
			
		}

        return Response::json($json);
	}

	public function signup () {

		$categories = Category::lists('name', 'id');

		return View::make('website.signup', compact('categories'));
	}

	public function signup_store () {

		if (!Request::ajax()) return Redirect::back();
		
		$json = array('success' => false, 'msg' => null, 'redirect' => null, 'text' => null);

		$data = Input::except('_token');

		try {

			DB::beginTransaction();

			$validator = Validator::make($data, User::$create_validate);

			if ($validator->fails()) throw new Exception($validator->messages());

			User::unique_user($data['email'], 0);

			$user = new User($data);

			$user->full_name = $data['first_name'] . ' ' . $data['last_name'];

			$user->password = Hash::make($data['password']);

			$user->user_type_id = 2; // Player

			$user->save();

			$player = new Player($data);

			$player->user_id = $user->id;

			$player->save();

			$user = User::find($user->id);

			$user->player_id = $player->id;

			$user->save();

			$tutor = new Tutor();

			$tutor->player_id = $player->id;

			$tutor->first_name = $data['tutor_first_name'];

			$tutor->last_name = $data['tutor_last_name'];

			$tutor->email = $data['tutor_email'];

			$tutor->phone_number = $data['tutor_phone_number'];

			$tutor->user_id = $user->id;

			$tutor->save();

			$email 		= HTML::entities(Input::get('email'));

			$password 	= HTML::entities(Input::get('password'));

			$credentials = array(
	            'email' => $email,
	            'password' => $password
	        );

         	if (Auth::attempt($credentials)) {

         	} else {

         		throw new Exception("No se pudo iniciar sesión, intentelo de nuevo más tarde.");
         		
         	}

			/* Mail::send('emails.signup-client', array('user' => $user), function($message) use ($email) {
    						
				$message->to($email)->subject('Bienvenido');
		 
		    }); */

			$json['redirect'] = route('panel.player.index');

			$json['title'] = 'El registro fue exitoso.';

			$json['text'] = 'Se ha registrado correctamente, redireccionando a panel ...';

			$json['success'] = true;

			DB::commit();
			
		} catch (Exception $e) {

			DB::rollBack();

			$json['success'] = false;

			$json['title'] = 'Error.';

			Log::error($e);

			if ($validator->fails()) {

				$errors = $validator->messages();

				foreach ($errors->all() as $error) {

			        $json['text'] .= $error.' - ';

			    }

			} else {

				$json['text'] = $e->getMessage();
			}
			
		}

        return Response::json($json);

	}

	/**
	 * Destroy a user's session
	 * GET /logout
	 *
	 * @return Response
	 */
	public function logout()
	{
	    Auth::logout();
	    
	    Session::flush();

	    return Redirect::to(route('session.signin'));
	}

	/**
	* Recovery password
	* POST / recovery
	*
	* @return Response
	*/
	public function recovery_password() 
	{
		if (!Request::ajax()) return Redirect::back();
		
		$json = array('success' => false, 'msg' => null);		

		$user = User::where('email', Input::get('email'))->select('id', 'email', 'first_name', 'last_name')->first();

		if(!empty($user)) {

			$token = str_random(7);

            $data = array('token' => $token, 'name' => $user->first_name . " " . $user->last_name);

            Mail::send('emails.recovery-password', $data, function($message) use ($user) {
                $message->to(Input::get('email'), 'Golf - Recuperar contraseña')
                		->subject('Golf - Recuperar contraseña');
            });	

            $user_recovery = new UserRecovery;

            $user_recovery->user_id = $user->id;
            
            $user_recovery->token 	= $token;

            $user_recovery->save();

			$json['msg'] = "Te enviamos un correo electrónico para que puedas restablecer tu contraseña.";

			$json['success'] = true;		

		} else {

			$json['msg'] = "La cuenta de correo electrónico que ha intentado requerir no existe.";
		}
       
        return Response::json($json);
	}	

	/*public function recoverypassword() 
	{
		return View::make('sessions.recovery');
	}*/


	public function recoverypassword ($id) {

		$recovery = UserRecovery::where('token', $id)->select('user_id')->first();

		if(!empty($recovery)) {

			$user = User::find($recovery->user_id);

			return View::make('users.recovery-password', compact('user'));
		}

		return View::make('users.recovery-password-error');
	}
	/**
	* New Password
	* POST / new
	*
	* @return Response
	*/
	public function password() 
	{
		if (!Request::ajax()) return Redirect::back();

		$json = array('success' => false, 'msg' => null);

		$user = User::find(Input::get('id'));

		if(!empty($user)) {

			$deleteToken = UserRecovery::where('user_id', Input::get('id'))->first();
			
			UserRecovery::destroy($deleteToken->id);

			$user->password = Hash::make(Input::get('password'));

			$user->save();

			$credentials = array(
	            'email' => $user->email,
	            'password' => Input::get('password')
	        );

			if (Auth::attempt($credentials)) {

        		$user = Auth::user();

	        	if ($user->active != 0) {
	        		
	        		$json['msg'] = 'Usuario inactivo';
	        		
	        		$json['success'] = false;

	        		Auth::logout();
		    
		    		Session::flush();

	        		//Logout

	        	} else {

	        		$json['msg'] = "Iniciando sesión ...";

					$json['success'] = true;

	        	}

	        } else {
	        	
	        	$json['msg'] = 'Acceso denegado: ';
	        }

			
		
		} else {

			$json['msg'] = "Solicitud cancelada.";
		}

        return Response::json($json);
	}

}