<?php

class AdminUserStageController extends BaseController {

	public function __construct () {

	}

	public function create ($stage_id, $tournament_id, $user_id) {

		return View::make('panel.admin.history._user_stage_create', compact('stage_id', 'tournament_id', 'user_id'));

	}

	public function store ()
	{

		if (!Request::ajax()) return Redirect::back();

		$json = array('title' => 'Habilitado',
					  'text' => 'Etapa fue habilitada',
					  'success' => true,
					  'redirect' => null);

		$data = Input::except('_token');

		try {

			DB::beginTransaction();

			$validator = Validator::make($data, UserStage::$create_validate, UserStage::$create_messages);

			if ($validator->fails()) throw new Exception($validator->messages());

			$user_stage = new UserStage($data);

			$user_stage->save();

			$user_id = $data['user_id'];

			$stage_id = $data['stage_id'];

			for ($i=1; $i <= 9; $i++)
			{
				$hole = new StageHoleScore;

				$hole->user_stage_id = $user_stage->id;

				$hole->user_id = $user_id;

				$hole->stage_id = $stage_id;

				$hole->day = 1;

				$hole->hole = $i;

				$hole->save();
			}

			for ($i=1; $i <= 9; $i++)
			{
				$hole = new StageHoleScore;

				$hole->user_stage_id = $user_stage->id;

				$hole->user_id = $user_id;

				$hole->stage_id = $stage_id;

				$hole->day = 2;

				$hole->hole = $i;

				$hole->save();
			}

			DB::commit();	
			
		} catch (Exception $e) {

			DB::rollBack();

			$json['success'] = false;

			$json['title'] = 'Error:';

			Log::error($e);

			if ($validator->fails()) {

				$errors = $validator->messages()->all();

				$json['text']  = '';

				foreach ($errors as $error) {

			        $json['text'] .= $error.' <br />';

			    }

			} else {

				$json['text'] = $e->getMessage();
			}
			
		}

		return Response::json($json);
	}

}