<?php

Route::get('/', array('as' => 'website.index', 'uses' => 'WebsiteController@index'));


Route::group(array('before' => 'auth'), function () {
	Route::get('session/logout', array('as' => 'logout', 'uses' => 'SessionController@logout'));
	Route::get('session/panel', array('as' => 'session.user_panel', 'uses' => 'SessionController@user_panel'));
});

Route::get('signin', array('as' => 'session.signin', 'uses' => 'SessionController@signin'));
Route::post('signin', array('as' => 'session.signin_store','uses' => 'SessionController@signin_store'));

Route::get('usuarios/recovery-password/{id}', array('as' => 'recovery_password_id','uses' => 'SessionController@recoverypassword'));
Route::post('password', array('as' => 'password', 'uses' => 'SessionController@password'));
Route::post('recovery', array('as' => 'session.recovery_password', 'uses' => 'SessionController@recovery_password'));

Route::get('registro', array('as' => 'session.signup', 'uses' => 'SessionController@signup'));
Route::post('signup', array('as' => 'session.signup_store', 'uses' => 'SessionController@signup_store'));

Route::group(array('prefix' => 'panel/player', 'before' => 'auth'), function () {
	Route::get('/', array('as' => 'panel.player.index', 'uses' => 'PlayersPanelController@index'));
	Route::resource('tutors', 'PlayerTutorsController');
	Route::get('tournaments', ['as' => 'panel.player.tournaments.index', 'uses' => 'PlayerTournamentController@index']);
	Route::get('tournaments/{tournament_id}/subscribe', ['as' => 'panel.player.tournaments.subscribe', 'uses' => 'PlayerTournamentController@subscribe']);
	Route::post('tournaments/{tournament_id}/subscribe', ['as' => 'panel.player.tournaments.subscribe_store', 'uses' => 'PlayerTournamentController@subscribe_store']);
	Route::delete('tournaments/user_tournaments/{id}', ['as' => 'panel.admin.user_tournaments.destroy', 'uses' => 'PlayerTournamentController@subscribe_destroy']);
	// Route::resource('tournaments', 'PlayerTournamentController');
	Route::get('history', ['as' => 'panel.player.history.index', 'uses' => 'PlayerHistoryController@index']);
	Route::resource('fields', 'PlayerFieldController');

	Route::get('user_stage/{stage_id}/{tournament_id}', ['as' => 'panel.player.user_stage.store', 'uses' => 'PlayerUserStageController@store']);
	Route::get('history/{user_stage_id}', ['as' => 'panel.player.user_stage.stage', 'uses' => 'PlayerUserStageController@stage']);

});

Route::group(array('prefix' => 'panel/admin', 'before' => 'auth'), function () {
	Route::get('/', array('as' => 'panel.admin.index', 'uses' => 'AdminPanelController@index'));
	Route::resource('players', 'AdminPlayerController');
	Route::resource('fields', 'AdminFieldController');
	Route::resource('tournaments', 'AdminTournamentController');

	Route::get('tournaments/{tournament_id}/stages/', ['as' => 'panel.admin.stages.index', 'uses' => 'AdminStageController@index']);
	Route::get('tournaments/{tournament_id}/stages/create', ['as' => 'panel.admin.stages.create', 'uses' => 'AdminStageController@create']);
	Route::post('tournaments/{tournament_id}/stages/', ['as' => 'panel.admin.stages.store', 'uses' => 'AdminStageController@store']);
	Route::get('tournaments/{tournament_id}/stages/{id}/edit', ['as' => 'panel.admin.stages.edit', 'uses' => 'AdminStageController@edit']);
	Route::put('tournaments/stages/{id}', ['as' => 'panel.admin.stages.update', 'uses' => 'AdminStageController@update']);
	Route::delete('tournaments/stages/{id}', ['as' => 'panel.admin.stages.destroy', 'uses' => 'AdminStageController@destroy']);

	Route::get('history/{tournament_id}', ['as' => 'panel.admin.history.index', 'uses' => 'AdminHistoryController@index']);
	Route::get('history/user_stage/create/{stage_id}/{tournament_id}/{user_id}', ['as' => 'panel.admin.user_stage.create', 'uses' => 'AdminUserStageController@create']);
	Route::post('history/user_stage', ['as' => 'panel.admin.user_stage.store', 'uses' => 'AdminUserStageController@store']);
});