<?php
class UsersTableSeeder extends Seeder {

	public function run()
	{
		Gender::truncate();

		$gender = new Gender;
		$gender->id = 1;
		$gender->name = 'Femenil';
		$gender->description = 'Tipo de genero femenil';
		$gender->save();

		$gender = new Gender;
		$gender->id = 2;
		$gender->name = 'Varonil';
		$gender->description = 'Tipo de genero varonil';
		$gender->save();

		User::truncate();

		$admin = new User;
		$admin->id = 1; 
		$admin->first_name 		= 'Administrator';
		$admin->last_name 		= 'admin';
		$admin->full_name 		= 'Administrator admin';
		$admin->password 		= Hash::make('password');
		$admin->email 			= 'admin@admin.com';
		$admin->user_type_id 	= 1;
		$admin->save();

		// Player

		$player_user = new User;
		$player_user->id = 2; 
		$player_user->first_name 	= 'Israel';
		$player_user->last_name 		= 'Martinez';
		$player_user->full_name 		= 'Israel Martinez';
		$player_user->password 		= Hash::make('password');
		$player_user->email 			= 'israel.martinez.vargas@gmail.com';
		$player_user->user_type_id 	= 2;
		$player_user->save();

		Player::truncate();

		$player = new Player();
		$player->user_id = $player_user->id;
		$player->phone_number = "(111) 111-1111";
		$player->birthday = '2016-03-10';
		$player->birthplace = 'Tijuana';
		$player->allergies = '';
		$player->category_id = 2;
		$player->save();

		$player_user = User::find($player_user->id);
		$player_user->player_id = $player->id;
		$player_user->save();

		Tutor::truncate();

		$tutor = new Tutor();
		$tutor->user_id = $player_user->id;
		$tutor->player_id = $player->id;
		$tutor->first_name = 'Tutor';
		$tutor->last_name = 'Tutor';
		$tutor->phone_number = '';
		$tutor->email = 'tutor@gmail.com';
		$tutor->save();

		Field::truncate();

		$field = new Field;
		$field->name = 'BAJA CALIFORNIA';
		$field->description = 'Campos de Baja California';
		$field->place = 'Tijuana Baja California';
		$field->save();

		Tournament::truncate();

		$tournament = new Tournament;
		$tournament->name = 'Torneo de Tijuana';
		$tournament->start_date = '2016-03-10';
		$tournament->finish_date = '2016-03-10';
		$tournament->description = 'Torneo de Tijuana';
		$tournament->field_id = $field->id;
		$tournament->status = 0;
		$tournament->gender_id = 1;
		$tournament->save();

		Stage::truncate();

		$stage = new Stage;
		$stage->tournament_id = $tournament->id;
		$stage->number = 1;
		$stage->field_id = $field->id;
		$stage->description = 'Primera etapa del torneo';
		$stage->start_date = '2016-03-10';
		$stage->finish_date = '2016-03-11';
		$stage->save();

		$stage = new Stage;
		$stage->tournament_id = $tournament->id;
		$stage->number = 2;
		$stage->field_id = $field->id;
		$stage->description = 'Segunda etapa del torneo';
		$stage->start_date = '2016-03-18';
		$stage->finish_date = '2016-03-19';
		$stage->save();

		// Categorias varonil

		$category = new Category;
		$category->name = '7 y menores Varonil';
		$category->gender_id = 2;
		$category->color = 'Verde';
		$category->save();

		$category = new Category;
		$category->name = '8 y 9 Varonil';
		$category->color = 'Amarillas especial';
		$category->gender_id = 2;
		$category->save();

		$category = new Category;
		$category->name = '10 y 11 Varonil';
		$category->color = 'Rojas';
		$category->gender_id = 2;
		$category->save();

		$category = new Category;
		$category->name = '12 y 13 Varonil';
		$category->color = 'Blancas o azules';
		$category->gender_id = 2;
		$category->save();

		$category = new Category;
		$category->name = '14 y 15 Varonil';
		$category->color = 'Negras';
		$category->gender_id = 2;
		$category->save();

		$category = new Category;
		$category->name = '16 y 18 Varonil';
		$category->color = 'Negras';
		$category->gender_id = 2;
		$category->save();

		// Categoria femenil

		$category = new Category;
		$category->name = '7 y menores Femenil';
		$category->gender_id = 1;
		$category->color = '';
		$category->save();

		$category = new Category;
		$category->name = '8 y 9 Femenil';
		$category->color = '';
		$category->gender_id = 1;
		$category->save();

		$category = new Category;
		$category->name = '10 y 11 Femenil';
		$category->color = 'Amarillas';
		$category->gender_id = 1;
		$category->save();

		$category = new Category;
		$category->name = '12 y 13 Femenil';
		$category->color = 'Std Rojas';
		$category->gender_id = 1;
		$category->save();

		$category = new Category;
		$category->name = '14 y 15 Femenil';
		$category->color = 'Blancas';
		$category->gender_id = 1;
		$category->save();

		$category = new Category;
		$category->name = '16 y 18 Femenil';
		$category->color = 'Blancas';
		$category->gender_id = 1;
		$category->save();

	}
}