<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTournamentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tournaments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name')->nullable();
			$table->string('start_date')->nullable();
			$table->string('finish_date')->nullable();
			$table->text('description')->nullable();
			$table->integer('status')->nullable(); /* 0 => Avalaible, 1 Cancelled */
			$table->integer('field_id')->nullable(); /* Disabled*/
			$table->integer('gender_id')->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tournaments');
	}

}
