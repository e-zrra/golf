<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStageHoleScoreTable extends Migration {

	public function up()
	{
		Schema::create('stage_hole_score', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('hole')->nullable();
			$table->string('yard')->nullable();
			$table->string('place')->nullable();
			$table->integer('user_id')->nullable();
			$table->integer('stage_id')->nullable();
			$table->integer('day')->nullable();
			$table->date('day_date')->nullable();
			$table->integer('user_stage_id')->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('stage_hole_score');
	}

}
