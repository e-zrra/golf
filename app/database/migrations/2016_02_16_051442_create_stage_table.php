<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('stages', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('tournament_id')->nullable();
			$table->integer('number')->nullable();
			$table->integer('field_id')->nullable();
			$table->text('description')->nullable();
			$table->string('start_date')->nullable();
			$table->string('finish_date')->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('stages');
	}

}
