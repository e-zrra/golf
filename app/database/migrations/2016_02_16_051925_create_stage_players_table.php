<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStagePlayersTable extends Migration {

	public function up()
	{
		Schema::create('user_stages', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('stage_id')->nullable();
			$table->integer('tournament_id')->nullable();
			$table->integer('field_id')->nullable();
			$table->integer('user_id')->nullable();
			$table->string('start_hole')->nullable();
			$table->string('hour')->nullable();
			$table->string('position')->nullable();
			$table->integer('status')->default(0);
			$table->text('description')->nullable();
			$table->integer('category_id')->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('user_stages');
	}

}
