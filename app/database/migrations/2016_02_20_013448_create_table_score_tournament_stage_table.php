<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableScoreTournamentStageTable extends Migration {

	public function up()
	{
		Schema::create('stage_score', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('start_hole')->nullable();
			$table->string('finish_hole')->nullable();
			$table->string('score')->nullable();
			$table->string('points')->nullable();
			$table->string('place')->nullable();
			$table->integer('user_id')->nullable();
			$table->integer('category_id')->nullable();
			$table->integer('stage_id')->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('stage_score');
	}

}
