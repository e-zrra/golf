<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTournamentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	// score_stage_tournaments
	public function up()
	{
		Schema::create('user_tournaments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('tournament_id')->nullable();
			$table->integer('user_id')->nullable();
			$table->integer('status')->nullable(); /* 0 => Suscribed, 1 => avalaible, 2 => unsuscribed*/
			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_tournaments');
	}

}
