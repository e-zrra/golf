jQuery.extend(jQuery.validator.messages, {
    required: "Este campo es requerido",
    email: "El campo debe tener formato de correo electrónico"
});

function show_dialog_error () {
	swal({
		title: "Conexión con el servidor.",
		text: 'Vaya, parece que hemos perdido la conexión con el servidor..<br><br>' + 
                'Este problema podría ser el resultado de:<br>' + 
                '&bull; No hay conexión a internet. <br>' + 
                '&bull; El servidor se encuentra en mantenimiento. <br>' + 
                "&bull; El servidor no puede responder en este momento. <br><br>",
        type: 'error',
		html: true
	});
}

$('.datetime-time').datetimepicker({
    locale: 'en',
    format: 'HH:mm'
});

$('.masked').inputmask({
    mask:'(999) 999-9999'
});

$('.datetime-date').datetimepicker({
    locale: 'es',
    format: 'YYYY-MM-DD',
    viewMode: 'years'
});

function form_submit (form) {

    var button  = $(form).find('input[type=submit]');
        text    = button.val(),
        type    = $(form).find('[name=_method]').val();

    if ( !type) type  = $(form).attr('method');
        
    $(form).ajaxSubmit({
        beforeSend: function() {
            button.val('Procesando ...').prop('disabled', true);
        },
        error: function(response) {
            show_dialog_error();
        },
        success: function(response) {
            if (response.success) {
                if (response.title) {
                    swal({
                        title: response.title,
                        text: (response.text) ? response.text : 'Procesando ...',
                        type: "success",
                        showConfirmButton: (response.show_button) ? true : false
                    });
                };
                if (response.redirect) {
                    setTimeout(function () {
                        window.location = response.redirect;
                    }, 2000);
                };
            } else {
                swal("Error", response.text, "error");
            }
        },
        complete: function() {
            return button.val(text).prop('disabled', false);
        },
        dataType: 'JSON',
        type: type
    });
}