
jQuery.extend(jQuery.validator.messages, {
    required: "Este campo es requerido",
    equalTo: "Por favor, introduzca el mismo valor de nuevo.",
});

$('#form-signup-player').validate({
    rules: {
        first_name: {
            required: true
        },
        last_name: {
            required: true
        },
        phone_number: {
        	required: true
        },
        birthday: {
        	required: true
        },
        birthplace: {
        	required: true
        },
        email: {
        	required: true,
            email: true
        },
        password: {
        	required: true
        },
        repeat_password: {
        	equalTo: "#password"
        },
        gender_id: {
        	required: true
        },
        category_id: {
        	required: true
        },
        tutor_first_name: {
        	required: true
        },
        tutor_last_name: {
        	required: true
        },
        tutor_phone_number: {
        	required: true
        },
        tutor_email: {
        	required: true,
            email: true
        }
    },
    submitHandler: function(form) {

        form_submit(form);

        /* var button  = $(form).find('input[type=submit]');
            text    = button.val();
        
        $(form).ajaxSubmit({
            beforeSend: function() {
                button.val('Registrando ...').prop('disabled', true);
            },
            error: function() {
            },
            success: function(response) {
                if (response.success) {
                    if (response.msg) {
                        swal({
                            title: 'Suscripción correctamente',
                            text: response.msg,
                            type: "success",
                            closeOnConfirm: false
                        });
                    };
                    if (response.redirect) {
                        setTimeout(function () {
                            window.location.href = response.redirect;
                        }, 3000);
                    }
                    form.reset();
                } else {
                    swal("Error", response.msg, "error");
                }
            },
            complete: function() {
                return button.val(text).prop('disabled', false);
            },
            dataType: 'JSON',
            type: "POST"
        }); */
    }
});