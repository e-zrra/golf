$('#form-signin').validate({
    rules: {
        email: {
            required: true,
            email: true
        },
        password: {
            required: true
        }
    },
    submitHandler: function(form) {
        form_submit(form);
    }
});