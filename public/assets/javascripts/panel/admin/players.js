$('#form-player-edit').validate({
    rules: {
        email: {
            required: true,
            email: true
        },
        '.required': {
            required: true
        }
    },
    submitHandler: function(form) {
        form_submit(form);
    }
});

$('#form-player-create').validate({
    rules: {
        email: {
            required: true,
            email: true
        },
        '.required': {
            required: true
        },
        password: {
            required: true
        }
    },
    submitHandler: function(form) {
        form_submit(form);
    }
});