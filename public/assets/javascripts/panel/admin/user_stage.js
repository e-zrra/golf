(function () {

	var $label = null;

	var $button = null;

	var $modal = $('#modal_user_stage');

	$('.btn_user_stage').on('click', function(event) {

		$button = $(this);

		$parent = $button.parent();

		$label = $parent.find('label');

		var url = $button.attr('src');

		if (url == null) {
			$modal.modal('hide'); return false;
		}

		$.ajax({
			url: url,
		  	type: "GET",
		  	dataType: "HTML",
		  	beforesend: function ()
		  	{
		  		$modal.find('.modal-body').text('Cargando ...');
		  	},
		  	error: function ()
		  	{
		  		console.log('Error');
		  		$modal.find('submit').hide();
		  	},
		  	success: function(data, textStatus, jqxhr)
		  	{
		    	$modal.find('.modal-body').html(data);
		  	},
		  	complete: function () {
		  	}
		});

	});

	$('#form-user-stage').validate({
	    rules: {
	        '.required': {
	            required: true
	        }
	    },
	    submitHandler: function(form) {
	        var button  = $(form).find('input[type=submit]'),
	        text    	= button.val(),
	        type    	= $(form).find('[name=_method]').val();

		    if ( !type) type  = $(form).attr('method');
		        
		    $(form).ajaxSubmit({
		        beforeSend: function() {
		            button.val('Procesando ...').prop('disabled', true);
		        },
		        error: function(response) {
		            show_dialog_error();
		        },
		        success: function(response) {
		            if (response.success)
		            {

		            	swal({
	                        title: response.title,
	                        text: response.text,
	                        type: "success",
	                        showConfirmButton: true
	                    });
		                
		            	$label.removeClass('label-warning').addClass('label-success').text('Habilitado');

		            	$button.remove();

		            	$modal.modal('hide');

		            	form.reset();

		            } else {

		                swal("Error", response.text, "error");
		            }
		        },
		        complete: function() {
		            return button.val(text).prop('disabled', false);
		        },
		        dataType: 'JSON',
		        type: type
		    });
	    }
	});


})();
//  