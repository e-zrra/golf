$('#form-player-subcribe').validate({
    rules: {
    },
    submitHandler: function(form) {
        var button  = $(form).find('input[type=submit]');
        text    = button.val(),
        type    = $(form).find('[name=_method]').val();

        if ( !type) type  = $(form).attr('method');
            
        $(form).ajaxSubmit({
            beforeSend: function() {
                button.val('Procesando ...').prop('disabled', true);
            },
            error: function(response) {
                show_dialog_error();
            },
            success: function(response) {
                if (response.success)
                {
                    if (response.title) {
                        swal({
                            title: response.title,
                            text: (response.text) ? response.text : 'Procesando ...',
                            type: "success",
                            showConfirmButton: (response.show_button) ? true : false
                        });
                    };

                    if (response.redirect) {
                        setTimeout(function () {
                            window.location = response.redirect;
                        }, 2000);
                    };
                    $(button).removeClass('btn-success');
                    $(button).addClass('btn-default');
                    $(button).prop('disabled', true);
                    $(button).val('Registrado');

                } else {
                    swal("Error", response.text, "error");
                    $(button).prop('disabled', false);
                    $(button).val(text);
                }
            },
            complete: function() {
                
            },
            dataType: 'JSON',
            type: type
        });
    }
});