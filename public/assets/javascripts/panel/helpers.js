(function () {

    jQuery.extend(jQuery.validator.messages, {
        required: "Este campo es requerido",
        email: "El campo debe tener formato de correo electrónico",
        email: "Por favor, introduce una dirección de correo electrónico válida.",
    });

})();

function show_dialog_error () {
	swal({
		title: "Conexión con el servidor.",
		text: 'Vaya, parece que hemos perdido la conexión con el servidor..<br><br>' + 
                'Este problema podría ser el resultado de:<br>' + 
                '&bull; No hay conexión a internet. <br>' + 
                '&bull; El servidor se encuentra en mantenimiento. <br>' + 
                "&bull; El servidor no puede responder en este momento. <br><br>",
        type: 'error',
		html: true
	});
}

(function () {

$('.datetime-time').datetimepicker({
    locale: 'en',
    format: 'HH:mm'
});

$('.datetime-time-lt').datetimepicker({
    locale: 'en',
    format: 'LT'
});

$('.masked').inputmask({
    mask:'(999) 999-9999'
});

$('.datetime-date').datetimepicker({
    locale: 'es',
    format: 'YYYY-MM-DD',
    viewMode: 'years'
});

})();

function form_submit (form) {

    var button  = $(form).find('input[type=submit]'),
        text    = button.val(),
        type    = $(form).find('[name=_method]').val();

    if ( !type) type  = $(form).attr('method');
        
    $(form).ajaxSubmit({
        beforeSend: function() {
            button.val('Procesando ...').prop('disabled', true);
        },
        error: function(response) {
            show_dialog_error();
        },
        success: function(response) {
            if (response.success)
            {
                if (response.title) {
                    swal({
                        title: response.title,
                        text: (response.text) ? response.text : 'Procesando ...',
                        type: "success",
                        showConfirmButton: (response.show_button) ? true : false
                    });
                };

                if (response.redirect) {
                    setTimeout(function () {
                        window.location = response.redirect;
                    }, 2000);
                };

                // $(form).find('input[type=password]').val('');

            } else {

                swal("Error", response.text, "error");
            }
        },
        complete: function() {
            return button.val(text).prop('disabled', false);
        },
        dataType: 'JSON',
        type: type
    });
}

(function () {

    'use strict';

    window.confirm_box = function (title, message, callback_function) {
        swal({
            title: title,
            text: message,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, eliminar",
            cancelButtonText: "No",
            closeOnConfirm: false },
            function (isConfirm) {
                if (isConfirm) {
                    swal({
                        title: 'Eliminando',
                        text: 'Procesando ...',
                        showConfirmButton: false
                    });
                    callback_function();
                }
            }
        );
    }

})();

(function () {
    $('.destroy-record').on('click', function(event) {
        event.preventDefault();
        var anchor = $(this);
        confirm_box('Eliminar registro', '¿Estas seguro en eliminar el registro?', function() {
            anchor.closest('form').submit();
        });
    });

})();